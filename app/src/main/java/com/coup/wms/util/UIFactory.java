package com.coup.wms.util;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * Created by 신동현 on 2017-10-20.
 */

public class UIFactory {
    public static void NumberPicker(Context a_context, DialogInterface.OnClickListener listener) {
        RelativeLayout linearLayout = new RelativeLayout(a_context);
        final NumberPicker aNumberPicker = new NumberPicker(a_context);
        aNumberPicker.setMaxValue(50);
        aNumberPicker.setMinValue(1);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(aNumberPicker, numPicerParams);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(a_context);
        alertDialogBuilder.setTitle("Select the number");
        alertDialogBuilder.setView(linearLayout);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Ok", listener);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void NumberPicker(Context a_context, final TextView tv) {
        RelativeLayout linearLayout = new RelativeLayout(a_context);
        final NumberPicker aNumberPicker = new NumberPicker(a_context);
        aNumberPicker.setMaxValue(50);
        aNumberPicker.setMinValue(0);
        aNumberPicker.setValue(Integer.parseInt(tv.getText().toString()));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(aNumberPicker, numPicerParams);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(a_context);
        alertDialogBuilder.setTitle("");
        alertDialogBuilder.setView(linearLayout);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("확인",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                tv.setText(Integer.toString(aNumberPicker.getValue()));
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public static void SendMail(final Context a_context, OnDHClickListner onDHClickListner) {
        if(!PermissionUtil.checkPermission(a_context, Manifest.permission.GET_ACCOUNTS))
            return;

        final OnDHClickListner _onDHClickListner = onDHClickListner;
        final EditText et = new EditText(a_context);
        et.setText(UIFactory.getUsername(a_context));
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(a_context);
        alertDialogBuilder.setView(et);

        alertDialogBuilder.setTitle("메일을 입력해주세요.");
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        _onDHClickListner.onDHClickLisnter(dialog, which, et.getText().toString());
                        dialog.cancel();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public interface OnDHClickListner {
        public void onDHClickLisnter(DialogInterface dialog, int which, String text);
    }

    private static String getUsername(Context context) {
        String strGmail ="";
        AccountManager manager = (AccountManager) context.getSystemService(ACCOUNT_SERVICE);
        Account[] list = manager.getAccounts();

        for (Account account : list) {

            String possibleEmail = account.name;
            String type = account.type;

            if (type.equals("com.google")) {
                strGmail = possibleEmail;

                Log.e("", "Emails: " + strGmail);
                break;
            }
        }
        return strGmail;
    }
}
