package com.coup.wms.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 신동현 on 2017-10-20.
 */

public class DHLib {
    public static String ToSlipDate(String date)
    {
        date = date.replace("년 ", "-");
        date = date.replace("월 ", "-");
        date = date.replace("일", "");
        date = date.replace("년", "-");
        date = date.replace("월", "-");
        return date;
    }

    public static String ToSlipDateNotUnderLine(String date)
    {
        date = date.replace("년 ", "");
        date = date.replace("월 ", "");
        date = date.replace("일", "");
        date = date.replace("년", "");
        date = date.replace("월", "");
        return date;
    }

    public static String ToHangleDate(String date)
    {
        try {
            SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date to = transFormat.parse(date);
            transFormat = new SimpleDateFormat("yyyy년 MM월 dd일");

            return transFormat.format(to);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }
}
