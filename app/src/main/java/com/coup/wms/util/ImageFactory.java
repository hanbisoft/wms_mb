package com.coup.wms.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

/**
 * Created by 신동현 on 2017-10-17.
 */

public class ImageFactory {

    public static Drawable ChangeDrawableSize(Context context, Drawable drawable, float width, float height)
    {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        Drawable new_drawable = new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, convertDpToPixel(width, context), convertDpToPixel(height, context), true));
//        drawable.setColorFilter(255, PorterDuff.Mode.ADD);
//        drawable.setColorFilter(0, PorterDuff.Mode.ADD);
//        drawable.setColorFilter(0, PorterDuff.Mode.ADD);
        return new_drawable;
    }

    static private int convertDpToPixel(float dp, Context context)
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int)px;
    }
/*
    public static boolean isEmptyImage(Context context, ImageView iv)
    {
        Drawable.ConstantState fDraw = iv.getDrawable().getConstantState();
        Drawable.ConstantState sDraw = context.getResources().getDrawable(R.drawable.icon_pic_none_resize).getConstantState();
        if(fDraw == sDraw)
            return true;
        return false;
    }

    public static void SetImageView(Context context, final ImageView iv, String gbn)
    {
        String url = COM.IMAGE_URL + "?user_id=" + UserInfo.User_Id(context) + "&img_gbn=" + gbn;
        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .error(R.drawable.icon_pic_none_resize).into(iv);
    }*/
}
