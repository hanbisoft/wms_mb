package com.coup.wms.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 신동현 on 2017-11-14.
 */

public class HttpRequest extends AsyncTask<String, Context, Integer> {
    Context _context;
    private ProgressBarUtil dialog;
    public HttpRequest(Context context){
        _context = context;
        dialog = new ProgressBarUtil(context,"전송 중입니다...");
    }

    @Override
    protected Integer doInBackground(String... params) {
        try {
            dialog.Show();
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(params[0])
                    .build();
            Response response = client.newCall(request).execute();
            return response.code();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        if(result == null) {
            Toast.makeText(_context, "전송실패", Toast.LENGTH_SHORT).show();
            dialog.Hide();
            return;
        }
        if(result== 200)
            Toast.makeText(_context, "전송완료", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(_context, "전송실패", Toast.LENGTH_SHORT).show();
        dialog.Hide();
        Log.e("ANSWER", "" + result);
    }
}
