package com.coup.wms.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.widget.ProgressBar;

/**
 * Created by 신동현 on 2017-11-14.
 */

public class ProgressBarUtil {
    ProgressDialog m_alertDialog;
    ProgressBar m_ProgressBar;
    Handler m_handler;

    ProgressBarUtil(final Context context, final String Message)
    {
        m_handler = new Handler();
        m_handler.post(new Runnable() {
            @Override
            public void run() {
                m_alertDialog = new ProgressDialog(context);
                m_alertDialog.setTitle("");
                m_alertDialog.setMessage(Message);
                m_alertDialog.setIndeterminate(false);
                m_alertDialog.setCancelable(false);
            }
        });
    }
    public void Show()
    {
        m_handler.post(new Runnable() {
            @Override
            public void run() {
                m_alertDialog.show();
            }
        });
    }

    public void Hide()
    {
        m_handler.post(new Runnable() {
            @Override
            public void run() {
                m_alertDialog.dismiss();
            }
        });
    }

}
