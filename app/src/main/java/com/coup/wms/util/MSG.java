package com.coup.wms.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by guyson on 2017-09-20.
 */

public class MSG {
    AlertDialog alertDialog;

    public void ShowDialog(Context context, String titleStr, String msg, DialogInterface.OnClickListener onClickListener)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        if( alertDialog != null && alertDialog.isShowing() ) return;
        // 제목셋팅
        alertDialogBuilder.setTitle(titleStr);

        // AlertDialog 셋팅
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("확인", onClickListener)
                .setNegativeButton("취소",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {
                                // 다이얼로그를 취소한다
                                dialog.cancel();
                            }
                        });

        // 다이얼로그 생성
        alertDialog = alertDialogBuilder.create();

        // 다이얼로그 보여주기
        alertDialog.show();
    }

    public void ShowDialog(Context context, String titleStr, String msg)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        if( alertDialog != null && alertDialog.isShowing() ) return;
        // 제목셋팅
        alertDialogBuilder.setTitle(titleStr);

        // AlertDialog 셋팅
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("확인",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {
                                // 다이얼로그를 취소한다
                                dialog.cancel();
                            }
                        });

        // 다이얼로그 생성
        alertDialog = alertDialogBuilder.create();

        // 다이얼로그 보여주기
        alertDialog.show();
    }
}
