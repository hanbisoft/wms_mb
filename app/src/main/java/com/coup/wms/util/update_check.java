package com.coup.wms.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.coup.wms.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class update_check extends Activity {

    public boolean UpdateCheck() {
        try {
            StringBuffer sb = new StringBuffer();
            PackageManager pm = getPackageManager();
            PackageInfo packageInfo;
            packageInfo = pm.getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
            //String version = packageInfo.versionName; //버전 네임
            int cVer = packageInfo.versionCode; //버전 코드

            String page = "http://wms.couplogbiz.com/COM/version.txt";
            URL url = new URL(page);

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(url.openStream()));

            String str = null;
            while ((str = reader.readLine()) != null) {
                sb.append(str);
            }

            //int cVer = Util.ToInt(curVer);
            int nVer = Integer.parseInt(sb.toString());

            if (cVer == nVer) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void gotoDownLoadPage()
    {
        // 사이트 열고
        Intent i = new Intent(Intent.ACTION_VIEW);
        Uri u = Uri.parse("http://wms.couplogbiz.com");
        i.setData(u);
        startActivity(i);

        // 종료
        System.exit(0);
    }

    public void ConfirmSave()
    {
        AlertDialog.Builder db = new AlertDialog.Builder(this);
        db.setTitle(R.string.app_name)
                .setMessage("프로그램이 업데이트 되었습니다\n다운로드 페이지로 이동하시겠습니까?")
                .setCancelable(true)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        gotoDownLoadPage();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }
}
