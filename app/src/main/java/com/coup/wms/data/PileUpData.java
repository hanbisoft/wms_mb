package com.coup.wms.data;

public class PileUpData {
    private boolean fisrt = true;
    private boolean chk = false;
    private String inboundNo;
    private String inboundSeq;
    private String barcode;
    private String itemNm;
    private String inlocCd;
    private String custCd;
    private int boxQty;
    private int eaQty;
    private int itemQty;
    private boolean scan = false;

    public String getInboundNo() {
        return inboundNo;
    }

    public void setInboundNo(String inboundNo) {
        this.inboundNo = inboundNo;
    }

    public String getInboundSeq() {
        return inboundSeq;
    }

    public void setInboundSeq(String inboundSeq) {
        this.inboundSeq = inboundSeq;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getItemNm() {
        return itemNm;
    }

    public void setItemNm(String itemNm) {
        this.itemNm = itemNm;
    }

    public String getInlocCd() {
        return inlocCd;
    }

    public void setInlocCd(String inlocCd) {
        this.inlocCd = inlocCd;
    }

    public int getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(int boxQty) {
        this.boxQty = boxQty;
    }

    public int getEaQty() {
        return eaQty;
    }

    public void setEaQty(int eaQty) {
        this.eaQty = eaQty;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    public boolean isScan() {
        return scan;
    }

    public void setScan(boolean scan) {
        this.scan = scan;
    }

    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) {
        this.chk = chk;
    }

    public boolean isFisrt() {
        return fisrt;
    }

    public void setFisrt(boolean fisrt) {
        this.fisrt = fisrt;
    }

    public String getCustCd() {
        return custCd;
    }

    public void setCustCd(String custCd) {
        this.custCd = custCd;
    }
}
