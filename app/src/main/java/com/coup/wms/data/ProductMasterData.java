package com.coup.wms.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.coup.wms.util.AselTran;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by 신동현 on 2017-10-18.
 */

public class ProductMasterData implements Parcelable {
    public String item_cd;
    public String item_nm;
    public String item_type;
    public String slip_dt;
    public ProductMasterData(SoapObject rs)
    {
        item_cd = AselTran.GetValue(rs, "item_cd");
        item_nm = AselTran.GetValue(rs, "item_nm");
        item_type = AselTran.GetValue(rs, "item_type");
        slip_dt = AselTran.GetValue(rs, "slip_dt");
    }

    protected ProductMasterData(Parcel in)
    {
        item_cd = in.readString();
        item_nm = in.readString();
        item_type = in.readString();
        slip_dt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(item_cd);
        dest.writeString(item_nm);
        dest.writeString(item_type);
        dest.writeString(slip_dt);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<ProductMasterData> CREATOR = new Creator<ProductMasterData>()
    {
        @Override
        public ProductMasterData createFromParcel(Parcel in)
        {
            return new ProductMasterData(in);
        }

        @Override
        public ProductMasterData[] newArray(int size)
        {
            return new ProductMasterData[size];
        }
    };
}
