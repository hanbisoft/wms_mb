package com.coup.wms.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.coup.wms.util.AselTran;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by 신동현 on 2017-10-18.
 */

public class FarmingReportData implements Parcelable {
    public String seq_no;
    public String slip_dt;
    public String item_cd;
    public String item_nm;
    public String workstep;
    public String remark;
    public String ownEmpMale_cnt;
    public String ownEmpFemale_cnt;
    public String outEmpMale_cnt;
    public String outEmpFemale_cnt;

    public FarmingReportData(SoapObject rs)
    {
        seq_no = AselTran.GetValue(rs, "seq_no");
        slip_dt = AselTran.GetValue(rs, "slip_dt");
        item_cd = AselTran.GetValue(rs, "item_cd");
        item_nm = AselTran.GetValue(rs, "item_nm");
        workstep = AselTran.GetValue(rs, "workstep");
        remark = AselTran.GetValue(rs, "remark");
        ownEmpMale_cnt = AselTran.GetValue(rs, "ownEmpMale_cnt");
        ownEmpFemale_cnt = AselTran.GetValue(rs, "ownEmpFemale_cnt");
        outEmpMale_cnt = AselTran.GetValue(rs, "outEmpMale_cnt");
        outEmpFemale_cnt = AselTran.GetValue(rs, "outEmpFemale_cnt");
    }

    protected FarmingReportData(Parcel in)
    {
        seq_no = in.readString();
        slip_dt = in.readString();
        item_cd = in.readString();
        item_nm = in.readString();
        workstep = in.readString();
        remark = in.readString();
        ownEmpMale_cnt = in.readString();
        ownEmpFemale_cnt = in.readString();
        outEmpMale_cnt = in.readString();
        outEmpFemale_cnt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(seq_no);
        dest.writeString(slip_dt);
        dest.writeString(item_cd);
        dest.writeString(item_nm);
        dest.writeString(workstep);
        dest.writeString(remark);
        dest.writeString(ownEmpMale_cnt);
        dest.writeString(ownEmpFemale_cnt);
        dest.writeString(outEmpMale_cnt);
        dest.writeString(outEmpFemale_cnt);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<FarmingReportData> CREATOR = new Creator<FarmingReportData>()
    {
        @Override
        public FarmingReportData createFromParcel(Parcel in)
        {
            return new FarmingReportData(in);
        }

        @Override
        public FarmingReportData[] newArray(int size)
        {
            return new FarmingReportData[size];
        }
    };
}
