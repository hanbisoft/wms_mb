package com.coup.wms.data;

public class InspectData {
    private boolean fisrt = true;
    private String inboundNo;
    private String inboundSeq;
    private String barcode;
    private String itemNm;
    private String inlocCd;
    private int boxQty;
    private int eaQty;
    private int itemQty;
    private String defectGbn = "";
    private boolean scan = false;
    private boolean chk = false;

    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) { this.chk = chk;}


    public boolean isFisrt() {
        return fisrt;
    }

    public void setFisrt(boolean fisrt) {
        this.fisrt = fisrt;
    }
    public String getInboundNo() {
        return inboundNo;
    }

    public void setInboundNo(String inboundNo) {
        this.inboundNo = inboundNo;
    }

    public String getInboundSeq() {
        return inboundSeq;
    }

    public void setInboundSeq(String inboundSeq) {
        this.inboundSeq = inboundSeq;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getItemNm() {
        return itemNm;
    }

    public void setItemNm(String itemNm) {
        this.itemNm = itemNm;
    }

    public String getInlocCd() {
        return inlocCd;
    }

    public void setInlocCd(String inlocCd) {
        this.inlocCd = inlocCd;
    }

    public int getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(int boxQty) {
        this.boxQty = boxQty;
    }

    public int getEaQty() {
        return eaQty;
    }

    public void setEaQty(int eaQty) {
        this.eaQty = eaQty;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    public String getDefectGbn() {
        return defectGbn;
    }

    public boolean isScan() {
        return scan;
    }


    public void setScan(boolean scan) {
        this.scan = scan;
    }

    public void setDefectGbn(String defectGbn) {
        this.defectGbn = defectGbn;
    }
}
