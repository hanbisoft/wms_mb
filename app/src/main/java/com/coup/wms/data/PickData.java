package com.coup.wms.data;

public class PickData {

    private boolean fisrt = true;
    private boolean chk = false;
    private String barcode;
    private String lotNo;
    private String itemNm;
    private String itemCd;
    private String outlocCd;
    private int boxQty;
    private int eaQty;
    private int itemQty;
    private boolean scan = false;

    public boolean isFisrt() {
        return fisrt;
    }

    public void setFisrt(boolean fisrt) {
        this.fisrt = fisrt;
    }

    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) {
        this.chk = chk;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getLotNo() {
        return lotNo;
    }

    public void setLotNo(String lotNo) {
        this.lotNo = lotNo;
    }

    public String getItemNm() {
        return itemNm;
    }

    public void setItemNm(String itemNm) {
        this.itemNm = itemNm;
    }

    public String getItemCd() {
        return itemCd;
    }

    public void setItemCd(String itemCd) {
        this.itemCd = itemCd;
    }

    public String getOutlocCd() {
        return outlocCd;
    }

    public void setOutlocCd(String outlocCd) {
        this.outlocCd = outlocCd;
    }

    public int getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(int boxQty) {
        this.boxQty = boxQty;
    }

    public int getEaQty() {
        return eaQty;
    }

    public void setEaQty(int eaQty) {
        this.eaQty = eaQty;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    public boolean isScan() {
        return scan;
    }

    public void setScan(boolean scan) {
        this.scan = scan;
    }
}
