package com.coup.wms.data;

/**
 * Created by yg102 on 2016-03-28.
 */
public class INTENT_KEY
{
    public static final int SETTING = 1;
    public static final int THEME=2;
	public static final int USER_LIST = 3;
	public static final int ADD_SCHEDULE = 4;
	public static final int PROFILE = 5;
	public static final int ADD_FARMING_REPORT = 6;
	public static final int ADD_FINANCE_INFORM = 7;
	public static final int ADD_PRODUCT_MASTER = 8;
	public static final int CREATE_SNS_POSTING = 9;
	public static final int CREATE_CHAT_GROUP = 10;
	public static final int CHATING = 11;
	public static final int FILE_PICKER = 12;
	public static final int PRINT = 13;
	public static final String FARM_GBN = "FARM_GBN";
	public static final String SCHEDULE_DAY = "SCHEDULEDAY";
	public static final String SEQ_NO = "SEQ_NO";
	public static final String FARM_PLANT = "PLANT";
	public static final String FARM_GARDENING = "GARDENING";
	public static final String FARM_LIFE = "LIFE";
	public static final String UP_PERIOD = "01";
	public static final String DOWN_PERIOD= "02";
}
