package com.coup.wms.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.coup.wms.util.AselTran;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by yg102 on 2016-03-22.
 */
public class UserInfo
{
    private static UserInfo instance;

    public String User_Nms;
    public String User_Ids;
    public String User_pwd;
    public String Comp_Cds;
    public String Comp_Nms;
	public String Dept_Cds;
	public String Dept_Nms;
	public String User_Gbns;
	public String app_name;
    public String Erp_App_Link;

	public static UserInfo getUserInfo(Context context)
    {
        if(instance==null)
        {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
            String User_Nms=settings.getString(Pref.PREF_KEY_USER_NMS, null);
            if(User_Nms==null)
                return null;

            instance=new UserInfo();
            instance.User_Nms = User_Nms;
            instance.User_Ids = settings.getString(Pref.PREF_KEY_USER_IDS,null);
            instance.User_pwd = settings.getString(Pref.PREF_KEY_USER_PWD,null);
            instance.Comp_Cds = settings.getString(Pref.PREF_KEY_COMP_CDS,null);
            instance.Comp_Nms = settings.getString(Pref.PREF_KEY_COMP_NMS,null);
	        //instance.Dept_Cds = settings.getString(Pref.PREF_KEY_DEPT_CDS,null);
	        instance.Dept_Nms = settings.getString(Pref.PREF_KEY_DEPT_NMS,null);
            instance.User_Gbns = settings.getString(Pref.PREF_KEY_USER_GBNS, null);
	        instance.app_name=settings.getString(Pref.PREF_KEY_APP_NAME, "SADARI Messenger");
            instance.Erp_App_Link = settings.getString(Pref.PREF_KEY_ERP_APP_LINK, null);
        }

        return instance;
    }

    public static void Clear(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
        instance = null;
    }


	public static void setUserInfo(Context context, SoapObject rs, String User_pwd)
	{
		String user_nm = AselTran.GetValue(rs, "user_nm");			// 컬럼2
		String comp_cd = AselTran.GetValue(rs, "comp_cd");
		String Comp_Nms = AselTran.GetValue(rs, "comp_nm");
		String User_Gbns = AselTran.GetValue(rs, "user_gbn");
		//String User_Ids = m_textID.getText().toString();
		String User_Ids=AselTran.GetValue(rs, "user_id");
		String appName = AselTran.GetValue(rs, "app_title");
        String dept_Nm = AselTran.GetValue(rs, "dept_nm");
        String erp_app = AselTran.GetValue(rs, "erp_app_link");     // ERP실행경로...

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

		SharedPreferences.Editor editor = settings.edit();
		editor.putString(Pref.PREF_KEY_USER_IDS, User_Ids);
		editor.putString(Pref.PREF_KEY_USER_PWD, User_pwd);
		editor.putString(Pref.PREF_KEY_USER_NMS, user_nm);
		editor.putString(Pref.PREF_KEY_COMP_CDS, comp_cd);
		editor.putString(Pref.PREF_KEY_COMP_NMS, Comp_Nms);
		editor.putString(Pref.PREF_KEY_USER_GBNS, User_Gbns);
		editor.putString(Pref.PREF_KEY_APP_NAME, appName);
        editor.putString(Pref.PREF_KEY_DEPT_NMS, dept_Nm);
        editor.putString(Pref.PREF_KEY_ERP_APP_LINK, erp_app);
		editor.apply();
	}

    public static String Comp_Cd(Context context)
    {
        return getUserInfo(context).Comp_Cds;
    }

    public static String User_Id(Context context)
    {
        return getUserInfo(context).User_Ids;
    }

    public static String User_pwd(Context context)
    {
        return getUserInfo(context).User_pwd;
    }

    public static String User_Nm(Context context)
    {
        return getUserInfo(context).User_Nms;
    }
}
