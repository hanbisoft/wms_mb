package com.coup.wms.data;

public class ComCode {
    private String commCd;
    private String codeNm;
    private String barcode;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public ComCode() {}

    public ComCode(String commCd, String codeNm) {
        this.commCd = commCd;
        this.codeNm = codeNm;
    }

    public String getCommCd() {
        return commCd;
    }

    public void setCommCd(String commCd) {
        this.commCd = commCd;
    }

    public String getCodeNm() {
        return codeNm;
    }

    public void setCodeNm(String codeNm) {
        this.codeNm = codeNm;
    }
}
