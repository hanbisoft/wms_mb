package com.coup.wms.data;

public class PickPopData {

    private boolean fisrt = true;
    private boolean chk = false;
    private String barcode;
    private String lotNo;
    private String pick_no;
    private String pick_seq;
    private String itemNm;
    private String itemCd;
    private String originInlocCd;
    private String inlocCd;
    private int boxQty;
    private int eaQty;
    private int itemQty;
    private boolean scan = false;
    private String outlocCd;
    private String couresNm;
    private String delvDegreeNm;

    public String getOutlocCd() {
        return outlocCd;
    }

    public void setOutlocCd(String outlocCd) {
        this.outlocCd = outlocCd;
    }

    public boolean isFisrt() {
        return fisrt;
    }

    public void setFisrt(boolean fisrt) {
        this.fisrt = fisrt;
    }

    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) {
        this.chk = chk;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getPick_no() {
        return pick_no;
    }

    public void setPick_no(String pick_no) {
        this.pick_no = pick_no;
    }

    public String getPick_seq() {
        return pick_seq;
    }

    public void setPick_seq(String pick_seq) {
        this.pick_seq = pick_seq;
    }

    public String getItemNm() {
        return itemNm;
    }

    public void setItemNm(String itemNm) {
        this.itemNm = itemNm;
    }

    public String getInlocCd() {
        return inlocCd;
    }

    public void setInlocCd(String inlocCd) {
        this.inlocCd = inlocCd;
    }

    public int getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(int boxQty) {
        this.boxQty = boxQty;
    }

    public int getEaQty() {
        return eaQty;
    }

    public void setEaQty(int eaQty) {
        this.eaQty = eaQty;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    public boolean isScan() {
        return scan;
    }

    public void setScan(boolean scan) {
        this.scan = scan;
    }

    public String getLotNo() {
        return lotNo;
    }

    public void setLotNo(String lotNo) {
        this.lotNo = lotNo;
    }

    public String getItemCd() {
        return itemCd;
    }

    public void setItemCd(String itemCd) {
        this.itemCd = itemCd;
    }

    public String getOriginInlocCd() {
        return originInlocCd;
    }

    public void setOriginInlocCd(String originInlocCd) {
        this.originInlocCd = originInlocCd;
    }

    public String getCouresNm() {
        return couresNm;
    }

    public void setCouresNm(String couresNm) {
        this.couresNm = couresNm;
    }

    public String getDelvDegreeNm() {
        return delvDegreeNm;
    }

    public void setDelvDegreeNm(String delvDegreeNm) {
        this.delvDegreeNm = delvDegreeNm;
    }
}
