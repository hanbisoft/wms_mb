package com.coup.wms.data;

import android.os.Bundle;

import java.text.SimpleDateFormat;

/**
 * Created by 신동현 on 2018-03-06.
 */

    public class FcmData {
    public static final String GCM_KEY_TITLE = "title";
    public static final String GCM_KEY_MESSAGE = "message";
    public static final String GCM_KEY_TYPE = "msgType";
    public static final String GCM_KEY_TIME = "time";

    public String title;
    public String type;
    public String message;


    public FcmData(Bundle bundle) {
        title = bundle.getString(GCM_KEY_TITLE);
        title = title.substring(title.indexOf("-") + 1);
        message = bundle.getString(GCM_KEY_MESSAGE);
        type = bundle.getString(GCM_KEY_TYPE);

        try {
            message = message.replace("^$@", "\n");
        } catch (Exception err) {
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd a hh:mm:ss");
        //    date = sdf.parse(bundle.getString(GCM_KEY_TIME));
        String msgType = bundle.getString("msgType");
    }
}

