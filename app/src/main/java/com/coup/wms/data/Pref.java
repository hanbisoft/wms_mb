package com.coup.wms.data;

/**
 * Created by yg102 on 2016-03-18.
 */
public class Pref
{
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String TOKEN_ID = "tokenId";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
	public static final String RECEIVED_GCM_MESSAGE= "gcm_message";

    public static final String PREF_KEY_USER_IDS="User_Ids";
    public static final String PREF_KEY_USER_PWD="User_pwd";
    public static final String PREF_KEY_USER_NMS="User_Nms";
    public static final String PREF_KEY_COMP_CDS="Comp_Cds";
    public static final String PREF_KEY_COMP_NMS="Comp_Nms";
    public static final String PREF_KEY_USER_GBNS="User_Gbns";

    public static final String PREF_KEY_THEME = "Theme";

	public static final String PREF_KEY_DEPT_CDS = "dept_cd";
	public static final String PREF_KEY_DEPT_NMS = "dept_nm";
	public static final String PREF_KEY_PROFILE_BACKGROUND = "profile_background";
	public static final String PREF_KEY_COMMENT = "comment";
	public static final String PREF_KEY_NOTIFY_GROUP_ID = "groupid";
	public static final String PREF_KEY_APP_NAME = "app_name";
	public static final String PREF_KEY_ALARM = "alarm";
    public static final String PREF_KEY_ERP_APP_LINK = "erp_app_link";
    public static final String PREF_KEY_LOCK = "lock";
	public static final String PREF_KEY_ALARM_TYPE = "alarm_type";
}
