package com.coup.wms.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.coup.wms.util.AselTran;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by 신동현 on 2017-10-20.
 */

public class FinancesInformData implements Parcelable {
    public String seq_no;
    public String slip_dt;
    public String item_cd;
    public String item_nm;
    public String finance_gbn;
    public String sale_gbn;
    public String grade;
    public String packing_unit;
    public String qty;
    public String price;
    public String amount;
    public String pay_gbn;
    public String remark;

    public FinancesInformData(SoapObject rs)
    {
        seq_no = AselTran.GetValue(rs, "seq_no");
        slip_dt = AselTran.GetValue(rs, "slip_dt");
        item_cd = AselTran.GetValue(rs, "item_cd");
        item_nm = AselTran.GetValue(rs, "item_nm");
        finance_gbn = AselTran.GetValue(rs, "finance_gbn");
        sale_gbn = AselTran.GetValue(rs, "sale_gbn");
        grade = AselTran.GetValue(rs, "grade");
        packing_unit = AselTran.GetValue(rs, "packing_unit");
        qty = AselTran.GetValue(rs, "qty");
        price = AselTran.GetValue(rs, "price");
        amount = AselTran.GetValue(rs, "amount");
        pay_gbn = AselTran.GetValue(rs, "pay_gbn");
        remark = AselTran.GetValue(rs, "remark");
    }

    protected FinancesInformData(Parcel in)
    {
        seq_no = in.readString();
        slip_dt = in.readString();
        item_cd = in.readString();
        item_nm = in.readString();
        finance_gbn = in.readString();
        sale_gbn = in.readString();
        grade = in.readString();
        packing_unit = in.readString();
        qty = in.readString();
        price = in.readString();
        amount = in.readString();
        pay_gbn = in.readString();
        remark = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(seq_no);
        dest.writeString(slip_dt);
        dest.writeString(item_cd);
        dest.writeString(item_nm);
        dest.writeString(finance_gbn);
        dest.writeString(sale_gbn);
        dest.writeString(grade);
        dest.writeString(packing_unit);
        dest.writeString(qty);
        dest.writeString(price);
        dest.writeString(amount);
        dest.writeString(pay_gbn);
        dest.writeString(remark);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<FinancesInformData> CREATOR = new Creator<FinancesInformData>()
    {
        @Override
        public FinancesInformData createFromParcel(Parcel in)
        {
            return new FinancesInformData(in);
        }

        @Override
        public FinancesInformData[] newArray(int size)
        {
            return new FinancesInformData[size];
        }
    };
}
