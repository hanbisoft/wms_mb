package com.coup.wms.data;

public class PileUpPopData {
    private boolean fisrt = true;
    private String inlocCd;
    private String inlocNm;
    private String barcode;
    private int itemQty;
    private boolean scan = false;
    private boolean chk = false;

    public boolean isFisrt() {
        return fisrt;
    }

    public void setFisrt(boolean fisrt) {
        this.fisrt = fisrt;
    }

    public String getInlocCd() {
        return inlocCd;
    }

    public void setInlocCd(String inlocCd) {
        this.inlocCd = inlocCd;
    }

    public String getInlocNm() {
        return inlocNm;
    }

    public void setInlocNm(String inlocNm) {
        this.inlocNm = inlocNm;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }


    public boolean isScan() {
        return scan;
    }

    public void setScan(boolean scan) {
        this.scan = scan;
    }

    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) {
        this.chk = chk;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }
}
