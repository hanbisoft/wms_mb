package com.coup.wms.data;

public class DivisionPopData {
    private boolean chk = false;
    private String divisionNo;
    private String divisionSeq;
    private String lotNo;
    private int itemQty;
    private int boxQty;
    private int eaQty;
    private String inlocCd;
    private String outlocCd;
    private String barcode;
    private boolean scan = false;

    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) {
        this.chk = chk;
    }

    public String getDivisionNo() {
        return divisionNo;
    }

    public void setDivisionNo(String divisionNo) {
        this.divisionNo = divisionNo;
    }

    public String getDivisionSeq() {
        return divisionSeq;
    }

    public void setDivisionSeq(String divisionSeq) {
        this.divisionSeq = divisionSeq;
    }

    public String getLotNo() {
        return lotNo;
    }

    public void setLotNo(String lotNo) {
        this.lotNo = lotNo;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    public int getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(int boxQty) {
        this.boxQty = boxQty;
    }

    public int getEaQty() {
        return eaQty;
    }

    public void setEaQty(int eaQty) {
        this.eaQty = eaQty;
    }

    public String getInlocCd() {
        return inlocCd;
    }

    public void setInlocCd(String inlocCd) {
        this.inlocCd = inlocCd;
    }

    public String getOutlocCd() {
        return outlocCd;
    }

    public void setOutlocCd(String outlocCd) {
        this.outlocCd = outlocCd;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public boolean isScan() {
        return scan;
    }

    public void setScan(boolean scan) {
        this.scan = scan;
    }
}
