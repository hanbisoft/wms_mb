package com.coup.wms.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.coup.wms.util.AselTran;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by 신동현 on 2017-10-18.
 */

public class ClosingData implements Parcelable {
    public String seq_no;
    public String slip_dt;
    public String finance_gbn;
    public String amount;

    public ClosingData(SoapObject rs)
    {
        seq_no = AselTran.GetValue(rs, "seq_no");
        slip_dt = AselTran.GetValue(rs, "slip_dt");
        finance_gbn = AselTran.GetValue(rs, "finance_gbn");
        amount = AselTran.GetValue(rs, "amount");
    }

    protected ClosingData(Parcel in)
    {
        seq_no = in.readString();
        slip_dt = in.readString();
        finance_gbn = in.readString();
        amount = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(seq_no);
        dest.writeString(slip_dt);
        dest.writeString(finance_gbn);
        dest.writeString(amount);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<ClosingData> CREATOR = new Creator<ClosingData>()
    {
        @Override
        public ClosingData createFromParcel(Parcel in)
        {
            return new ClosingData(in);
        }

        @Override
        public ClosingData[] newArray(int size)
        {
            return new ClosingData[size];
        }
    };
}
