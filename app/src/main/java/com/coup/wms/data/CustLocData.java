package com.coup.wms.data;

public class CustLocData {
    private String cust_cd;
    private String cust_nm;
    private String loc_cd;
    private String loc_nm;
    private String barcode;

    public String getCust_cd() {
        return cust_cd;
    }

    public void setCust_cd(String cust_cd) {
        this.cust_cd = cust_cd;
    }

    public String getCust_nm() {
        return cust_nm;
    }

    public void setCust_nm(String cust_nm) {
        this.cust_nm = cust_nm;
    }

    public String getLoc_cd() {
        return loc_cd;
    }

    public void setLoc_cd(String loc_cd) {
        this.loc_cd = loc_cd;
    }

    public String getLoc_nm() {
        return loc_nm;
    }

    public void setLoc_nm(String loc_nm) {
        this.loc_nm = loc_nm;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
