package com.coup.wms.data;

public class DefectData {
    private String cust_cd;
    private String cust_nm;
    private String comm_cd;
    private String code_nm;

    public String getCust_cd() {
        return cust_cd;
    }

    public void setCust_cd(String cust_cd) {
        this.cust_cd = cust_cd;
    }

    public String getCust_nm() {
        return cust_nm;
    }

    public void setCust_nm(String cust_nm) {
        this.cust_nm = cust_nm;
    }

    public String getComm_cd() {
        return comm_cd;
    }

    public void setComm_cd(String comm_cd) {
        this.comm_cd = comm_cd;
    }

    public String getCode_nm() {
        return code_nm;
    }

    public void setCode_nm(String code_nm) {
        this.code_nm = code_nm;
    }
}


