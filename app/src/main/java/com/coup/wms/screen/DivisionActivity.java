package com.coup.wms.screen;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.R;
import com.coup.wms.adapter.ListViewAdapterDivision;
import com.coup.wms.data.DivisionData;
import com.coup.wms.data.DivisionPopData;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by 신동현 on 2018-11-09.
 */
public class DivisionActivity extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener, AdapterView.OnItemClickListener {

    DivisionActivity activity;

    String barcodeDivision = "";
    int barcodeDivisionIndex = -1;
    EditText etScan = null;

    TextView sDate;
    final Calendar cldr = Calendar.getInstance();
    DatePickerDialog picker;
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);
    int dayOfWeek = cldr.get(Calendar.DAY_OF_WEEK);
    String userid;
    String _param;
    public static Context CONTEXT;
    View ping;

    SharedPreferences setting;
    SharedPreferences.Editor editor;

    ListView listView;
    ListViewAdapterDivision adapter;
    ArrayList<DivisionData> list = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_division);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        출처: https://bitsoul.tistory.com/173 [Happy Programmer~]
        activity = this;

        Intent intent = getIntent();
        sDate = findViewById(R.id.sdate);
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        tuserid.setText(userid + "님");
        CONTEXT = this;

        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();
        ping = findViewById(R.id.ping);

        _param = userid;
        setDate();
        //onClick(null);
        //searchInboundGbn();
        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                Intent intent = new Intent(DivisionActivity.this, loginActivity.class);
                                startActivity(intent);
                               onStop();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        etScan = findViewById(R.id.tv_scan);

        listView = findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new ListViewAdapterDivision(this, list);
        listView.setOnItemClickListener(this);




        searchList();
    }

    @Override
    public void onResume() {
        super.onResume();

       //onClick(null);

    }

    private void searchList() {
        new SOAPLoadTask(this, this).execute("usp_MOB_OutDivision_list", SOAPLoadTask.convertParams(
                "LIST",
                sDate.getText().toString(),
                sDate.getText().toString()));
    }

    @Override
    public void onGetResult(String processer, SoapObject _result) {
        switch (processer) {

            case "usp_MOB_OutDivision_list" : {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        DivisionData data = new DivisionData();
                        data.setItemCd(AselTran.GetValue(rs, "item_cd"));
                        data.setItemNm(AselTran.GetValue(rs, "item_nm"));
                        data.setItemQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setBoxQty((int)Double.parseDouble(AselTran.GetValue(rs, "box_qty")));
                        data.setEaQty((int)Double.parseDouble(AselTran.GetValue(rs, "ea_qty")));
                        data.setBarcode(AselTran.GetValue(rs, "barcode"));

                        list.add(data);
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                //adapter.notifyDataSetChanged();
                //listView.invalidateViews();
                //listView.refreshDrawableState();

                adapter = new ListViewAdapterDivision(this, list);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(this);

                int list_amount = _result.getPropertyCount();
                Log.wtf("ik",String.valueOf(list_amount));
                if(list_amount==1){
                    Toast.makeText(this, "작업 리스트가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            }

        }

    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                searchList();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_search: {
                searchList();

            }
        }
    }

    public void backbtn(View v) {
        onBackPressed();
    }


    public void setDate(){

        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new Date());
//        if(dayOfWeek==1)
//        cal.add(Calendar.DAY_OF_YEAR, -2);
//        else if(dayOfWeek==2)
//            cal.add(Calendar.DAY_OF_YEAR, -3);
//        else
//            cal.add(Calendar.DAY_OF_YEAR, -1);

        SimpleDateFormat fm = new SimpleDateFormat(
                "yyyy-MM-dd");
        String strDate = fm.format(cal.getTime());
        sDate.setText(strDate);

    }

    public void setDate(View v) {

        switch (v.getId()) {
            case R.id.sdate:
                picker = new DatePickerDialog(DivisionActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                sDate.setText(year + "-" + String.format("%02d",monthOfYear + 1) + "-" + String.format("%02d",dayOfMonth));
                                //onClick(null);
                                //searchInboundGbn();A
                                searchList();
                            }
                        }, year, month, day);
                picker.show();
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        goPopup(i);
    }

    public void goPopup(int position) {
        DivisionData data = list.get(position);
        Intent intent = new Intent(DivisionActivity.this, PopupDivision.class);
        intent.putExtra("userid", userid);
        intent.putExtra("position", position);
        intent.putExtra("sdate", sDate.getText().toString());
        intent.putExtra("item_cd", data.getItemCd());
        intent.putExtra("item_nm", data.getItemNm());
        startActivityForResult(intent, 1);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getKeyCode() == 277 || event.getKeyCode() == 278) {
            etScan.setText("");
            etScan.requestFocus();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if(event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            if(!"".equals(etScan.getText().toString())) {
                String barcode = etScan.getText().toString().replaceAll("(\r\n|\r|\n|\n\r)", "");
                Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                //etScan.setText("");
                for(DivisionData data : list) {
                    data.setScan(false);
                }
                for(int i=0; i<list.size(); i++) {
                    DivisionData data = list.get(i);
                    if(barcode.equals(data.getBarcode())) {
                        //Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                        data.setScan(true);
                        barcodeDivision = barcode;
                        listView.smoothScrollToPosition(i);
                        barcodeDivisionIndex = i;
                        adapter.notifyDataSetChanged();
                        goPopup(barcodeDivisionIndex);
                        break;
                    }
                }
                return false;
            }
        }


        return super.dispatchKeyEvent(event);

    }



}
