package com.coup.wms.screen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.adapter.ListViewAdapter_popup_item;
import com.coup.wms.data.ItemData;

import com.coup.wms.R;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 신동현 on 2018-11-12.
 */
public class popup_order extends Activity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener  {
    TextView torder_nm, tcust_nm, tarrival_nm, taddr, titem_nm, torder_qty, tprice_amt;
    String _param, order_no,sdate;
    List<String> _param2 = null;
    ListView m_oListView;
    int pos;
    String userid;
    private ArrayList<ItemData> m_oData = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_order);

        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        pos = intent.getIntExtra("position", 0);
        sdate = intent.getStringExtra("sdate");
        _param= userid;
        onClick(null);

        m_oListView = (ListView)findViewById(R.id.listitem);
    }


    @Override
    public void onGetResult(String processer, SoapObject _result)
    {

        if(order_no != null){
            ArrayList<ItemData> oData = new ArrayList<>();
            try {
                for (int i = 1; i < _result.getPropertyCount(); i++) {
                    SoapObject rs = (SoapObject) _result.getProperty(i);


                    String item_nm = AselTran.GetValue(rs, "item_nm");                // 결과
                    String order_qty = AselTran.GetValue(rs, "order_qty");
                    String price_amt = AselTran.GetValue(rs, "price_amt");


                    ItemData oItem = new ItemData();
                    oItem.item_nm = item_nm;
                    oItem.order_qty = order_qty;
                    oItem.price_amt = price_amt;


                    oData.add(oItem);

                    order_no = null;
                }

                ListAdapter oAdapter = new ListViewAdapter_popup_item(this, R.layout.popup_order, oData);
                m_oListView.setAdapter(oAdapter);
            }
            catch (Exception e) {
                Toast.makeText(this,  e.getMessage() , Toast.LENGTH_SHORT).show();
            }
        }
        else {
            try {

                SoapObject rs = (SoapObject) _result.getProperty(pos + 1);

                String cust_nm = AselTran.GetValue(rs, "cust_nm");                // 결과
                String arrival_nm = AselTran.GetValue(rs, "arrival_nm");
                String addr = AselTran.GetValue(rs, "addr");
                String order_nm = AselTran.GetValue(rs, "order_no");
                order_no = AselTran.GetValue(rs, "order_no");

                torder_nm = (TextView) findViewById(R.id.order_nm);
                tcust_nm = (TextView) findViewById(R.id.cust_nm);
                tarrival_nm = (TextView) findViewById(R.id.arrival_nm);
                taddr = (TextView) findViewById(R.id.addr);

                torder_nm.setText(order_nm);
                tcust_nm.setText(cust_nm);
                tarrival_nm.setText(arrival_nm);
                taddr.setText(addr);

                _param2 = new ArrayList<String>();
                _param2.add(_param);
                _param2.add(order_no);
                _param2.add(sdate);
                onClick(null);

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }




    @Override
    public void onError(SoapObject _result)
    {
        Toast.makeText(this,"통신에 실패하였습니다.",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1){
            if(resultCode==RESULT_OK){

            }
        }
    }

    @Override
    public void onClick(View v) {
        if(order_no!=null)
            new SOAPLoadTask((SOAPLoadTask.OnResultListener)this,this).execute("usp_MOB_DeliveryTarget_list", SOAPLoadTask.convertParams(_param2));
        else{
        new SOAPLoadTask((SOAPLoadTask.OnResultListener)this,this).execute("usp_MOB_DeliveryTarget_list", SOAPLoadTask.convertParams(_param, "", sdate));}
    }


}



