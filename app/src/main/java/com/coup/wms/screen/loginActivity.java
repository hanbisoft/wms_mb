package com.coup.wms.screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.BuildConfig;
import com.coup.wms.R;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;


public class loginActivity extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener {
    TextView m_textID;


    Button m_btnLogin;


    TextView m_textPassword;
    Toast m_toast;

    CheckBox chk_auto;
    SharedPreferences setting;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        chk_auto = (CheckBox) findViewById(R.id.Auto_Login);
        setting = getSharedPreferences("setting", 0);
        editor= setting.edit();

        m_textPassword = findViewById(R.id.edt_password);
        m_textID = findViewById(R.id.edt_userid);
        m_btnLogin = findViewById(R.id.login_btn);
        if (BuildConfig.DEBUG)
        {
            //m_textID.setText("test2@hotmail.com");
            //m_textPassword.setText("1234");
        }

        try {
            m_btnLogin.setOnClickListener(this);
            if(setting.getBoolean("chk_auto", false)){
                m_textID.setText(setting.getString("User_Id", null));
                m_textPassword.setText(setting.getString("User_pwd", null));
                chk_auto.setChecked(true);
                onClick(null);
            }
        }
        catch (Exception err)
        {

        }

    }



    public void onClick(View _v)
    {
        if(m_textID.length()==0)
        {
            if(m_toast!=null)
                m_toast.cancel();
            m_toast= Toast.makeText(this,"아이디를 입력하세요.",Toast.LENGTH_SHORT);
            m_toast.show();

            m_textID.requestFocus();
            return;
        }

        if(m_textPassword.length()==0)
        {
            if(m_toast!=null)
                m_toast.cancel();

            m_toast= Toast.makeText(this,"비밀번호를 입력하세요.",Toast.LENGTH_SHORT);
            m_toast.show();

            m_textPassword.requestFocus();
            return;
        }

        if(chk_auto.isChecked()){

            Toast.makeText(this, "자동 로그인", Toast.LENGTH_SHORT).show();
            editor.putBoolean("chk_auto", true);


        }
        if(!(chk_auto.isChecked())){
            editor.clear();
            editor.commit();
        }


        List<String> _param = new ArrayList<String>();
        _param.add(m_textID.getText().toString());
        _param.add(m_textPassword.getText().toString());
        _param.add("");

        //프로시저 == 틀 제목
        new SOAPLoadTask((SOAPLoadTask.OnResultListener) this, this).execute("usp_MOB_GetLoginInfo", SOAPLoadTask.convertParams(_param));
    }

    @Override
    public void onGetResult(String processer, SoapObject _result)
    {
        SoapObject rs = (SoapObject)_result.getProperty(1);

        String Flag = AselTran.GetValue(rs, "RFLAG");				// 결과
        String rMsg = AselTran.GetValue(rs, "RMSG");				// 메세지
        if (!Flag.equals("T")) {
            Toast.makeText(this, rMsg, Toast.LENGTH_SHORT).show();
            return;
        }
        editor.putString("User_Id",m_textID.getText().toString());
        editor.putString("User_pwd",m_textPassword.getText().toString());
        editor.commit();
        Intent intent = new Intent(loginActivity.this, MainActivity.class);
        intent.putExtra("userid", m_textID.getText().toString());
        startActivity(intent);
        finish();

    }

    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this,_result+"통신에 실패하였습니다.",Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
