package com.coup.wms.screen;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.R;
import com.coup.wms.adapter.ListViewAdapterDivisionPop;
import com.coup.wms.adapter.ListViewAdapterPileupPop;
import com.coup.wms.data.DivisionPopData;
import com.coup.wms.data.PileUpPopData;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;

/**
 * Created by 신동현 on 2018-11-12.
 */
public class PopupPileUp extends Activity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener  {
    PopupPileUp activity;
    public static Context CONTEXT;

    String barcodeLoc = "";
    int barcodeLocIndex = -1;
    EditText etScan = null;

    TextView tvItemNm, tvBoxEaQty;
    Button btnSave;
    String _param, order_no,sdate;

    int pos;
    String userid;
    String itemNm, cust_cd, inbound_no, inbound_seq;
    int ea_qty, box_qty;

    String slipNo;

    ListView listView;
    private ArrayList<PileUpPopData> list = new ArrayList<>();
    ListViewAdapterPileupPop adapter;

    //CheckBox chkAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_pileup);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        activity = this;
        CONTEXT = this;

        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        pos = intent.getIntExtra("position", 0);
        sdate = intent.getStringExtra("sdate");
        itemNm = intent.getStringExtra("item_nm");
        cust_cd = intent.getStringExtra("cust_cd");
        inbound_no = intent.getStringExtra("inbound_no");
        inbound_seq = intent.getStringExtra("inbound_seq");
        ea_qty = intent.getIntExtra("ea_qty", 0);
        box_qty = intent.getIntExtra("box_qty", 0);
        _param= userid;

        tvItemNm = findViewById(R.id.tv_item_nm);
        tvItemNm.setText(itemNm);

        tvBoxEaQty = findViewById(R.id.tv_box_ea_qty);
        tvBoxEaQty.setText(box_qty + " / " + ea_qty);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);

        listView = (ListView)findViewById(R.id.listview);
        adapter = new ListViewAdapterPileupPop(this, list);
        listView.setAdapter(adapter);

        etScan = findViewById(R.id.tv_scan);

        searchList();
    }

    private void searchList() {
        new SOAPLoadTask(this, this).execute("usp_MOB_PileUp_list", SOAPLoadTask.convertParams(
                "LOC", "", "", "", cust_cd, String.valueOf(ea_qty)));
    }

    private void getSlipNo() {
        new SOAPLoadTask(this, this).execute("usp_COM_GetSlipNo", SOAPLoadTask.convertParams("", "INP_PileUp", sdate.replace("-", ""), ""));
    }


    @Override
    public void onGetResult(String processer, SoapObject _result)
    {
        switch (processer) {
            case "usp_MOB_PileUp_list": {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        PileUpPopData data = new PileUpPopData();
                        data.setChk("Y".equals(AselTran.GetValue(rs, "chk")));
                        data.setBarcode(AselTran.GetValue(rs, "barcode"));
                        data.setItemQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setInlocCd(AselTran.GetValue(rs, "inloc_cd"));

                        list.add(data);
                    }

                    adapter = new ListViewAdapterPileupPop(this, list);
                    listView.setAdapter(adapter);
                }
                catch (Exception e) {
                    Toast.makeText(this,  e.getMessage() , Toast.LENGTH_SHORT).show();
                }
                break;
            }

            case "usp_COM_GetSlipNo" : {
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);
                        slipNo = AselTran.GetValue(rs, "slip_no");
                    }

                    SaveTask task = new SaveTask();
                    task.execute();

                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                break;
            }
        }
    }


    public void saveEnd() {
        setResult(RESULT_OK);
        finish();
    }


    @Override
    public void onError(SoapObject _result)
    {
        Toast.makeText(this,"통신에 실패하였습니다.",Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save: {
                int cnt = 0;
                int tot = 0;
                for(int i=0; i<list.size(); i++) {
                    PileUpPopData data = list.get(i);
                    if(data.isChk()) {
                        cnt++;
                        tot += data.getItemQty();
                    }
                }
                if(cnt == 0) {
                    Toast.makeText(this, "저장할 작업이 없습니다.", Toast.LENGTH_SHORT).show();
                }
                else if(tot != ea_qty) {
                    Toast.makeText(this, "체크된 로케이션의 합계수량은 입고수량과 같아야 합니다.", Toast.LENGTH_SHORT).show();
                }
                else {
                    getSlipNo();
                }
                break;
            }
        }
    }

    public class SaveTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(PopupPileUp.this);

        @Override
        protected void onPreExecute() {


            for(int i=0; i<list.size(); i++) {
                PileUpPopData data = list.get(i);
                if(data.isChk()) {
                    new SOAPLoadTask(activity, activity).execute("usp_MOB_PileUp_iud", SOAPLoadTask.convertParams("IN", userid, slipNo, inbound_no, inbound_seq, data.getInlocCd(), String.valueOf(data.getItemQty())));
                }
            }

            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("저장 중입니다..");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
            Toast.makeText((PopupPileUp) PopupPileUp.CONTEXT, "저장 했습니다.", Toast.LENGTH_SHORT).show();
            ((PopupPileUp) PopupPileUp.CONTEXT).saveEnd();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getKeyCode() == 277 || event.getKeyCode() == 278) {
            etScan.setText("");
            etScan.requestFocus();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if(event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            if(!"".equals(etScan.getText().toString())) {
                String barcode = etScan.getText().toString().replaceAll("(\r\n|\r|\n|\n\r)", "");
                Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                //etScan.setText("");
                for(PileUpPopData data : list) {
                    data.setScan(false);
                }

                for(int i=0; i<list.size(); i++) {
                    PileUpPopData data = list.get(i);
                    if(barcode.equals(data.getBarcode())) {
                        //Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                        data.setScan(true);
                        data.setChk(true);
                        barcodeLoc = barcode;
                        listView.smoothScrollToPosition(i);
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
                return false;
            }
        }


        return super.dispatchKeyEvent(event);

    }


}



