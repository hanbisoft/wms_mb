package com.coup.wms.screen;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.R;
import com.coup.wms.adapter.ComCodeAdapter;
import com.coup.wms.adapter.ListViewAdapterPileUp;
import com.coup.wms.data.CustLocData;
import com.coup.wms.data.DivisionData;
import com.coup.wms.data.PileUpData;
import com.coup.wms.util.AselTran;
import com.coup.wms.data.ComCode;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by 신동현 on 2018-11-09.
 */
public class PileUpActivity extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {

    PileUpActivity activity;

    String slipNo;

    String barcodeInbound = "";
    int barcodeInboundIndex = -1;
    String barcodeLoc = "";
    EditText etScan = null;

    TextView sDate;
    final Calendar cldr = Calendar.getInstance();
    DatePickerDialog picker;
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);
    int dayOfWeek = cldr.get(Calendar.DAY_OF_WEEK);
    String userid;
    String _param;
    public static Context CONTEXT;
    View ping;

    //Button btnSave;
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    Spinner spInboundGbn;
    Spinner spCustCd;

    ComCodeAdapter adInboundGbn;
    ArrayList<ComCode> listInboundGbn = new ArrayList<>();
    ComCodeAdapter adCustCd;
    ArrayList<ComCode> listCustCd = new ArrayList<>();

    ListView listView;
    ListViewAdapterPileUp adapter;
    ArrayList<PileUpData> list = new ArrayList<>();

    //ArrayList<CustLocData> listCustLoc = new ArrayList<>();

   // CheckBox chkAll;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pileup);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        activity = this;

        Intent intent = getIntent();
        sDate = findViewById(R.id.sdate);
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        tuserid.setText(userid + "님");
        CONTEXT = this;
        //btnSave = findViewById(R.id.btn_save);

        etScan = findViewById(R.id.tv_scan);

        spInboundGbn = findViewById(R.id.sp_inbound_gbn);
        adInboundGbn = new ComCodeAdapter(this, listInboundGbn);
        spInboundGbn.setAdapter(adInboundGbn);
        spInboundGbn.setOnItemSelectedListener(this);

        spCustCd = findViewById(R.id.sp_cust_cd);
        adCustCd = new ComCodeAdapter(this, listCustCd);
        spCustCd.setAdapter(adCustCd);
        spCustCd.setOnItemSelectedListener(this);

        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();
        ping = findViewById(R.id.ping);
      //  pingtest();
        //btnSave.setOnClickListener(this);

        _param = userid;
        setDate();
        //onClick(null);
        //searchInboundGbn();
        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                Intent intent = new Intent(PileUpActivity.this, loginActivity.class);
                                startActivity(intent);
                               onStop();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        listView = (ListView) findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new ListViewAdapterPileUp(this, list);
        listView.setOnItemClickListener(this);


//        chkAll = findViewById(R.id.chk_all);
//        chkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                for(int i=0; i<list.size(); i++) {
//                    list.get(i).setChk(b);
//                }
//                if(adapter != null) {
//                    adapter.notifyDataSetChanged();
//                }
//            }
//        });

        searchInboundGbn();
    }

    @Override
    public void onResume() {
        super.onResume();

       //onClick(null);

    }

    private void searchInboundGbn() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetInboundGbn", SOAPLoadTask.convertParams("LIST"));
    }

    private void searchCustCd() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetCustCd", SOAPLoadTask.convertParams("LIST"));
    }

    private void searchCustLoc() {
        ComCode custCd = (ComCode)spCustCd.getSelectedItem();
        new SOAPLoadTask(this, this).execute("usp_MOB_GetCustLoc", SOAPLoadTask.convertParams("LIST", custCd != null ? custCd.getCommCd() : ""));
    }

    private void searchList() {
        ComCode inboundGbn = (ComCode)spInboundGbn.getSelectedItem();
        ComCode custCd = (ComCode)spCustCd.getSelectedItem();

        new SOAPLoadTask(this, this).execute("usp_MOB_PileUp_list", SOAPLoadTask.convertParams(
                "LIST",
                sDate.getText().toString(),
                sDate.getText().toString(),
                inboundGbn != null ? inboundGbn.getCommCd() : "",
                custCd != null ? custCd.getCommCd() : "" ));
    }

    private void getSlipNo() {
        new SOAPLoadTask(this, this).execute("usp_COM_GetSlipNo", SOAPLoadTask.convertParams("", "INP_PileUp", sDate.getText().toString().replace("-", ""), ""));
    }

    @Override
    public void onGetResult(String processer, SoapObject _result) {
        switch (processer) {
            case "usp_MOB_GetInboundGbn" : {
                listInboundGbn.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");
                        listInboundGbn.add( new ComCode(comm_cd, code_nm) );
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                adInboundGbn = new ComCodeAdapter(this, listInboundGbn);
                spInboundGbn.setAdapter(adInboundGbn);
                spInboundGbn.setOnItemSelectedListener(this);
                //adInboundGbn.notifyDataSetChanged();

                searchCustCd();
                break;
            }

            case "usp_MOB_GetCustCd" : {
                listCustCd.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");
                        listCustCd.add( new ComCode(comm_cd, code_nm) );
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                adCustCd = new ComCodeAdapter(this, listCustCd);
                spCustCd.setAdapter(adCustCd);
                spCustCd.setOnItemSelectedListener(this);
                //adCustCd.notifyDataSetChanged();

                //searchCustLoc();
                searchList();

                break;
            }

//            case "usp_MOB_GetCustLoc" : {
//                listCustLoc.clear();
//                try {
//                    for (int i = 1; i < _result.getPropertyCount(); i++) {
//                        SoapObject rs = (SoapObject) _result.getProperty(i);
//
//                        CustLocData data = new CustLocData();
//                        data.setCust_cd(AselTran.GetValue(rs, "cust_cd"));
//                        data.setCust_nm(AselTran.GetValue(rs, "cust_nm"));
//                        data.setLoc_cd(AselTran.GetValue(rs, "loc_cd"));
//                        data.setLoc_nm(AselTran.GetValue(rs, "loc_nm"));
//                        data.setBarcode(AselTran.GetValue(rs, "barcode"));
//
//                        listCustLoc.add( data );
//                    }
//                } catch (Exception e) {
//                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//
//                searchList();
//
//                break;
//            }

            case "usp_MOB_PileUp_list" : {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        PileUpData data = new PileUpData();
                        data.setInboundNo(AselTran.GetValue(rs, "Inbound_no"));
                        data.setInboundSeq(AselTran.GetValue(rs, "Inbound_seq"));
                        data.setBarcode(AselTran.GetValue(rs, "barcode"));
                        data.setItemNm(AselTran.GetValue(rs, "item_nm"));
                        data.setCustCd(AselTran.GetValue(rs, "cust_cd"));
                        data.setItemQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setBoxQty((int)Double.parseDouble(AselTran.GetValue(rs, "box_qty")));
                        data.setEaQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setInlocCd(AselTran.GetValue(rs, "inloc_cd"));

                        list.add(data);
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                //adapter.notifyDataSetChanged();
                //listView.invalidateViews();
                //listView.refreshDrawableState();

                adapter = new ListViewAdapterPileUp(this, list);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(this);

                int list_amount = _result.getPropertyCount();
                Log.wtf("ik",String.valueOf(list_amount));
                if(list_amount==1){
                    Toast.makeText(this, "작업 리스트가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            }

            case "usp_COM_GetSlipNo" : {
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);
                        slipNo = AselTran.GetValue(rs, "slip_no");
                    }

                    SaveTask task = new SaveTask();
                    task.execute();

                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                break;
            }

        }

    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                searchList();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save: {
                int cnt = 0;
                for(int i=0; i<list.size(); i++) {
                    PileUpData data = list.get(i);
                    if(data.isChk()) {
                        cnt++;
                    }
                }
                if(cnt == 0) {
                    Toast.makeText(this, "저장할 작업이 없습니다.", Toast.LENGTH_SHORT).show();
                }
                else {
                    getSlipNo();
                }
                break;
            }
            case R.id.btn_search: {
                searchList();

            }
        }
    }

    public void backbtn(View v) {
        onBackPressed();
    }


    public void setDate(){

        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new Date());
//        if(dayOfWeek==1)
//        cal.add(Calendar.DAY_OF_YEAR, -2);
//        else if(dayOfWeek==2)
//            cal.add(Calendar.DAY_OF_YEAR, -3);
//        else
//            cal.add(Calendar.DAY_OF_YEAR, -1);

        SimpleDateFormat fm = new SimpleDateFormat(
                "yyyy-MM-dd");
        String strDate = fm.format(cal.getTime());
        sDate.setText(strDate);

    }

    public void setDate(View v) {

        switch (v.getId()) {
            case R.id.sdate:
                picker = new DatePickerDialog(PileUpActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                sDate.setText(year + "-" + String.format("%02d",monthOfYear + 1) + "-" + String.format("%02d",dayOfMonth));
                                //onClick(null);
                                //searchInboundGbn();A
                                searchList();
                            }
                        }, year, month, day);
                picker.show();
                break;

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getKeyCode() == 277 || event.getKeyCode() == 278) {
            etScan.setText("");
            etScan.requestFocus();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if(event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            if(!"".equals(etScan.getText().toString())) {
                String barcode = etScan.getText().toString().replaceAll("(\r\n|\r|\n|\n\r)", "");
                Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                //etScan.setText("");
                if("PU".equals(barcode.substring(0, 2))) {
                    for(PileUpData data : list) {
                        data.setScan(false);
                    }
                    for(int i=0; i<list.size(); i++) {
                        PileUpData data = list.get(i);
                        if(barcode.equals(data.getBarcode())) {
                            //Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                            data.setScan(true);
                            barcodeInbound = barcode;
                            barcodeInboundIndex = i;
                            adapter.notifyDataSetChanged();
                            listView.smoothScrollToPosition(barcodeInboundIndex);
                            goPopup(barcodeInboundIndex);
                            break;
                        }
                    }
                }
//                else if("LC".equals(barcode.substring(0, 2))) {
//                    if(barcodeInboundIndex > -1) {
//                        PileUpData data = list.get(barcodeInboundIndex);
//
//                        for(CustLocData loc : listCustLoc) {
//                            if(barcode.equals(loc.getBarcode())) {
//                                data.setChk(true);
//                                data.setInlocCd(loc.getLoc_cd());
//                                adapter.notifyDataSetChanged();
//                                //listView.smoothScrollToPosition(barcodeInboundIndex);
//                                break;
//                            }
//                        }
//                    }
//                }
                return false;
            }
        }


        return super.dispatchKeyEvent(event);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        searchList();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        goPopup(i);
    }

    public void goPopup(int position) {
        PileUpData data = list.get(position);
        Intent intent = new Intent(PileUpActivity.this, PopupPileUp.class);
        intent.putExtra("userid", userid);
        intent.putExtra("position", position);
        intent.putExtra("sdate", sDate.getText().toString());
        intent.putExtra("item_nm", data.getItemNm());
        intent.putExtra("cust_cd", data.getCustCd());
        intent.putExtra("inbound_no", data.getInboundNo());
        intent.putExtra("inbound_seq", data.getInboundSeq());
        intent.putExtra("ea_qty", data.getEaQty());
        intent.putExtra("box_qty", data.getBoxQty());
        startActivityForResult(intent, 1);
    }


    public class SaveTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(PileUpActivity.this);

        @Override
        protected void onPreExecute() {


            for(int i=0; i<list.size(); i++) {
                PileUpData data = list.get(i);
                if(data.isChk()) {
                    new SOAPLoadTask(activity, activity).execute("usp_MOB_PileUp_iud", SOAPLoadTask.convertParams("IN", userid, slipNo, data.getInboundNo(), data.getInboundSeq(), data.getInlocCd(), String.valueOf(data.getItemQty())));
                }
            }

            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("저장 중입니다..");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
            Toast.makeText((PileUpActivity) PileUpActivity.CONTEXT, "저장 했습니다.", Toast.LENGTH_SHORT).show();
            ((PileUpActivity) PileUpActivity.CONTEXT).searchList();
        }
    }
}
