package com.coup.wms.screen;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.R;
import com.coup.wms.adapter.ListViewAdapter_take_back;
import com.coup.wms.data.ItemData;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class take_back extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener {
    String _param, userid;
    int list_amount, pos = -1;
    ListView m_oListView;
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    ArrayList<ItemData> oData = new ArrayList<>();
    EditText start_date,end_date;
    DatePickerDialog picker;
    public static final String PREFS_NAME = "MyPrefsFile1";
    public CheckBox dontShowAgain;
    ListAdapter oAdapter;
    TextView nowtime;
    final Calendar cldr = Calendar.getInstance();
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);
    View ping;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.take_back);
        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        nowtime = (TextView) findViewById(R.id.nowTime);
        ping = findViewById(R.id.ping);
      //  pingtest();
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


        String getTime = sdf.format(date);
        nowtime.setText(getTime);
        tuserid.setText(userid + "님");

        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();

        _param = userid;

        CheckTypesTask task = new CheckTypesTask();
        task.execute();

        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.change_pw:
                                Intent intent = new Intent(take_back.this, pw_change.class);
                                intent.putExtra("userid", userid);
                                startActivity(intent);
                                break;
                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                intent = new Intent(take_back.this, loginActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        AlertDialog.Builder alert_confirm = new AlertDialog.Builder(take_back.this);
        alert_confirm.setMessage("회수 처리 하시겠습니까?").setCancelable(false).setPositiveButton("전송",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(pos==-1){
                            Toast.makeText(take_back.this, "반품 확인할 상품을 선택해주세요", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        confirm_back();
                    }
                }).setNegativeButton("취소",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 'No'
                        return;
                    }
                });

        final AlertDialog alert = alert_confirm.create();

        findViewById(R.id.btn_saving).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert.show();
            }
        });

        findViewById(R.id.send_msg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckTypesTask task = new CheckTypesTask();
                task.execute();
            }
        });


        m_oListView = (ListView) findViewById(R.id.listview);


        //얼럿다이얼로그
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        LayoutInflater adbInflater = LayoutInflater.from(this);
        View eulaLayout = adbInflater.inflate(R.layout.checkbox, null);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        final String skipMessage = settings.getString("skipMessage", "NOT checked");

        dontShowAgain = (CheckBox) eulaLayout.findViewById(R.id.skip);



    }
    private class CheckTypesTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(
                take_back.this);

        @Override
        protected void onPreExecute() {


             onClick(null);
            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("불러오는 중입니다..");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
        }
    }

    private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long l_position) {
            for (int i = 0; i < list_amount - 1; i++) {
                oData.get(i).backpos = false;
            }

            oData.get(position).backpos = true;
            pos = position;
            ((ListViewAdapter_take_back) oAdapter).notifyDataSetChanged();
        }
    };


    @Override
    public void onGetResult(String processer, SoapObject _result) {

            try {
                oData.clear();
                for (int i = 1; i < _result.getPropertyCount(); i++) {
                    SoapObject rs = (SoapObject) _result.getProperty(i);

                    String retReq_no = AselTran.GetValue(rs, "retReq_no");
                    String retReq_seq = AselTran.GetValue(rs, "retReq_seq");                // 결과
                    String cust_nm = AselTran.GetValue(rs, "cust_nm");
                    String arrival_cd = AselTran.GetValue(rs, "arrival_cd");
                    String arrival_nm = AselTran.GetValue(rs, "arrival_nm");
                    String item_cd = AselTran.GetValue(rs, "item_cd");
                    String item_nm = AselTran.GetValue(rs, "item_nm");
                    String item_qty = AselTran.GetValue(rs, "item_qty");

                    if (retReq_seq.equals("") || retReq_seq.equals("0"))
                        retReq_seq = "0";


                    ItemData oItem = new ItemData();
                    oItem.retReq_no = retReq_no;
                    oItem.retReq_seq = retReq_seq;
                    oItem.cust_nm  = cust_nm;
                    oItem.arrival_nm = arrival_nm;
                    oItem.item_cd = item_cd;
                    oItem.item_nm = item_nm;
                    oItem.item_qty  = item_qty;

                    oData.add(oItem);
                }

                list_amount = _result.getPropertyCount();
                if(list_amount==1){
                    Toast.makeText(this, "반품 리스트가 없습니다.", Toast.LENGTH_LONG).show();
                }
                oAdapter = new ListViewAdapter_take_back(oData);

                m_oListView.setAdapter(oAdapter);
                m_oListView.setOnItemClickListener(mItemClickListener);

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + "ERROR", Toast.LENGTH_SHORT).show();
            }

        }



    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        new SOAPLoadTask( this, this).execute("usp_MOB_ReturnTarget_list", SOAPLoadTask.convertParams(_param));
    }
    public void confirm_back(){
        SOAPLoadTask.OnResultListener onresult = new SOAPLoadTask.OnResultListener() {
            @Override
            public void onGetResult(String processer, SoapObject _result) {
                try {
                        SoapObject rs = (SoapObject) _result.getProperty(1);
                        String ret = AselTran.GetValue(rs, "ret");

                        if (ret.equals("-1"))
                            Toast.makeText(take_back.this, "오류가 발생했습니다", Toast.LENGTH_SHORT).show();
                        else {
                            Toast.makeText(take_back.this, "반품 확인 되었습니다", Toast.LENGTH_SHORT).show();
                            CheckTypesTask task = new CheckTypesTask();
                            task.execute();
                        }

                }catch (Exception e){

                }
            }
        };
        SOAPLoadTask.OnErrorListener onError = new SOAPLoadTask.OnErrorListener() {
            @Override
            public void onError(SoapObject _result) {
                Toast.makeText(take_back.this, "오류가 발생했습니다", Toast.LENGTH_SHORT).show();
            }
        };

        new SOAPLoadTask(onresult, onError).execute("usp_MOB_ReturnTarget_iud", SOAPLoadTask.convertParams(oData.get(pos).retReq_no, oData.get(pos).retReq_seq));

    }
    public void backbtn(View v) {
        onBackPressed();
    }
    public void pingtest(){
        String host = "mall.ppang.biz";
        String cmd = "ping -c 1 -W 10 "+ host;
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            proc.waitFor();
            int result = proc.exitValue();
            if(result == 1)
                ping.setBackgroundColor(Color.parseColor("#FFBB00"));
            else if(result == 2)
                ping.setBackgroundColor(Color.parseColor("#FF3636"));


        } catch (Exception e) {
            Log.e("ping", e.getMessage());
        }
    }

}




