package com.coup.wms.screen;

/**
 * Created by 신동현 on 2018-11-12.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.coup.wms.R;
import com.coup.wms.adapter.ListViewAdapter_popup_item;
import com.coup.wms.data.ItemData;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.COM;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class popup_sign extends Activity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener {
    SignaturePad signaturePad;
    Button saveButton, clearButton;
    TextView torder_nm, tcust_nm, tarrival_nm, taddr, titem_nm, torder_qty, tprice_amt, nowtime;
    String _param, order_no;
    int pos, list_amount, endsu;
    List<String> _param2 = null;
    ListView m_oListView;
    String order_seq;
    Bitmap signImage = null;
    String userid,ocheck;

    public static Context CONTEXT;
    ArrayList<ItemData> oData = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_sign);
        CONTEXT = this;
        signaturePad = (SignaturePad) findViewById(R.id.signaturePad);
        saveButton = (Button) findViewById(R.id.saveButton);
        clearButton = (Button) findViewById(R.id.clearButton);
        nowtime = (TextView) findViewById(R.id.nowTime);
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd  hh:mm:ss");

        String getTime = sdf.format(date);
        nowtime.setText(getTime);
        //disable both buttons at start
        saveButton.setEnabled(false);
        clearButton.setEnabled(false);
        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        String data = intent.getStringExtra("data");
        _param = userid;
        pos = intent.getIntExtra("position", 0);
        onClick(null);

        m_oListView = (ListView) findViewById(R.id.listitem);

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                saveButton.setEnabled(true);
                clearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                saveButton.setEnabled(false);
                clearButton.setEnabled(false);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //write code for saving the signature here
                Toast.makeText(popup_sign.this, "수취 확인하였습니다.", Toast.LENGTH_SHORT).show();
                signImage = signaturePad.getSignatureBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                signImage.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();
                Log.wtf("bytesize", String.valueOf(b.length));
                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                SaveAndDialog(encodedImage);


                // new SOAPLoadTask(this, this).execute("usp_MOB_DeliveryFix_img", SOAPLoadTask.convertParams(_param3));
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
            }
        });
    }

    //메인 쓰레드 X
   public void SaveAndDialog(final String encodedImage)
    {

        for (int i = 0; i < list_amount-1; i++) {
            new SOAPLoadTask((SOAPLoadTask.OnResultListener) this, this).execute("usp_MOB_DeliveryFix_iud", SOAPLoadTask.convertParams(order_no, order_seq, oData.get(i).order_qty));
        }

        new Thread(new Runnable() {
            public void run()
            {
                // 저장 시작.

                SaveSign(encodedImage);

            }
        }).start();
    }


    boolean SaveSign(String encodedImage) {
        String rec_msg = "";
        try {
            SoapObject request = new SoapObject(COM.NAMESPACE, "AndroidDeliverySignSave" + "");

            //string comp_cd, string slip_no, string svc_grd, string strImgByte
            request.addProperty("comp_cd", "10");
            request.addProperty("slip_no", "2018110800101");    // 주문번호
            request.addProperty("svc_grd", "10");
            request.addProperty("strImgByte",encodedImage);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE adt = new HttpTransportSE(COM.URLSRV);
            //adt.call(COM.SOAP_ACTION + COM.SAVE_METHOD, envelope);
            adt.call("http://tempuri.org/AndroidDeliverySignSave", envelope);

            Object ob = envelope.getResponse();
            rec_msg = ob.toString();

            String[] param = rec_msg.split("\\^");    //. | ^ 등 특수한경우에는 이스케이프한다.
            String workFlag = param[0];
            String Msg = param[1];


            if (!workFlag.equals("T")) {
                return false;
            }

            finish();

            return true;
        } catch (Exception err) {
            err.getStackTrace();
            err.printStackTrace();
            return false;
        }
    }


    @Override
    public void onGetResult(String processer, SoapObject _result) {

        if (ocheck == null && order_no != null ) {

            try {
                for (int i = 1; i < _result.getPropertyCount(); i++) {

                    SoapObject rs = (SoapObject) _result.getProperty(i);


                    String item_nm = AselTran.GetValue(rs, "item_nm");                // 결과
                    String order_qty = AselTran.GetValue(rs, "order_qty");
                    String price_amt = AselTran.GetValue(rs, "price_amt");


                    ItemData oItem = new ItemData();

                    oItem.item_nm = item_nm;
                    oItem.order_qty = order_qty.substring(0,order_qty.indexOf("."));
                    oItem.price_amt = price_amt;



                    oData.add(oItem);


                }
                ocheck = "kek";
                list_amount = _result.getPropertyCount();
                Log.wtf("list", String.valueOf(list_amount));
                ListAdapter oAdapter = new ListViewAdapter_popup_item(this, R.layout.row_order_item, oData);
                m_oListView.setAdapter(oAdapter);
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        else if(ocheck != null ){
            try {




            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        else if (signImage != null) {
            try {

                SoapObject rs = (SoapObject) _result.getProperty(pos + 1);

                Intent intent = new Intent();
                intent.putExtra("num", "1");
                setResult(RESULT_OK, intent);
                finish();


            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
        else {
            try {

                SoapObject rs = (SoapObject) _result.getProperty(pos + 1);


                String cust_nm = AselTran.GetValue(rs, "cust_nm");                // 결과
                String arrival_nm = AselTran.GetValue(rs, "arrival_nm");
                String addr = AselTran.GetValue(rs, "addr");
                String order_nm = AselTran.GetValue(rs, "order_no");


                order_seq = AselTran.GetValue(rs, "seq_no");
                order_no = AselTran.GetValue(rs, "order_no");

                torder_nm = (TextView) findViewById(R.id.order_nm);
                tcust_nm = (TextView) findViewById(R.id.cust_nm);
                tarrival_nm = (TextView) findViewById(R.id.arrival_nm);
                taddr = (TextView) findViewById(R.id.addr);

                torder_nm.setText(order_nm);
                tcust_nm.setText(cust_nm);
                tarrival_nm.setText(arrival_nm);
                taddr.setText(addr);

                _param2 = new ArrayList<String>();
                _param2.add(_param);
                _param2.add(order_no);
                onClick(null);

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

            }
        }
    }



    @Override
    public void onClick(View v) {
        if (order_no != null)
            new SOAPLoadTask((SOAPLoadTask.OnResultListener) this, this).execute("usp_MOB_DeliveryTarget_list", SOAPLoadTask.convertParams(_param2));
        else {
            new SOAPLoadTask((SOAPLoadTask.OnResultListener) this, this).execute("usp_MOB_DeliveryTarget_list", SOAPLoadTask.convertParams(_param));
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //바깥레이어 클릭시 안닫히게
        if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
            return false;
        }
        return true;
    }


}