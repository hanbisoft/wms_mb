package com.coup.wms.screen;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.R;
import com.coup.wms.adapter.ListViewAdapterDivisionPop;
import com.coup.wms.data.DivisionPopData;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;

/**
 * Created by 신동현 on 2018-11-12.
 */
public class PopupDivision extends Activity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener  {
    PopupDivision activity;
    public static Context CONTEXT;

    String barcodeDivision = "";
    int barcodeDivisionIndex = -1;
    EditText etScan = null;

    TextView tvItemNm;
    Button btnSave;
    String _param, order_no,sdate;

    int pos;
    String userid;
    String itemCd, itemNm;

    ListView listView;
    private ArrayList<DivisionPopData> list = new ArrayList<>();
    ListViewAdapterDivisionPop adapter;

    CheckBox chkAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_division);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        activity = this;
        CONTEXT = this;

        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        pos = intent.getIntExtra("position", 0);
        sdate = intent.getStringExtra("sdate");
        itemCd = intent.getStringExtra("item_cd");
        itemNm = intent.getStringExtra("item_nm");
        _param= userid;

        tvItemNm = findViewById(R.id.tv_item_nm);
        tvItemNm.setText(itemNm);
        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);

        listView = (ListView)findViewById(R.id.listview);
        adapter = new ListViewAdapterDivisionPop(this, list);
        listView.setAdapter(adapter);

        etScan = findViewById(R.id.tv_scan);

        chkAll = findViewById(R.id.chk_all);
        chkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for(int i=0; i<list.size(); i++) {
                    list.get(i).setChk(b);
                }
                if(adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        searchList();
    }

    private void searchList() {
        new SOAPLoadTask(this, this).execute("usp_MOB_OutDivision_list", SOAPLoadTask.convertParams(
                "LOC",
                sdate,
                sdate,
                itemCd));
    }


    @Override
    public void onGetResult(String processer, SoapObject _result)
    {
        switch (processer) {
            case "usp_MOB_OutDivision_list": {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        DivisionPopData data = new DivisionPopData();
                        data.setBarcode(AselTran.GetValue(rs, "barcode"));
                        data.setItemQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setBoxQty((int)Double.parseDouble(AselTran.GetValue(rs, "box_qty")));
                        data.setEaQty((int)Double.parseDouble(AselTran.GetValue(rs, "ea_qty")));
                        data.setInlocCd(AselTran.GetValue(rs, "inloc_cd"));
                        data.setOutlocCd(AselTran.GetValue(rs, "outloc_cd"));
                        data.setDivisionNo(AselTran.GetValue(rs, "division_no"));
                        data.setDivisionSeq(AselTran.GetValue(rs, "division_seq"));
                        data.setLotNo(AselTran.GetValue(rs, "lot_no"));

                        list.add(data);
                    }

                    adapter = new ListViewAdapterDivisionPop(this, list);
                    listView.setAdapter(adapter);
                }
                catch (Exception e) {
                    Toast.makeText(this,  e.getMessage() , Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }


    public void saveEnd() {
        setResult(RESULT_OK);
        finish();
    }


    @Override
    public void onError(SoapObject _result)
    {
        Toast.makeText(this,"통신에 실패하였습니다.",Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save: {
                int cnt = 0;
                for(int i=0; i<list.size(); i++) {
                    DivisionPopData data = list.get(i);
                    if(data.isChk()) {
                        cnt++;
                    }
                }
                if(cnt == 0) {
                    Toast.makeText(this, "저장할 작업이 없습니다.", Toast.LENGTH_SHORT).show();
                }
                else {
                    SaveTask task = new SaveTask();
                    task.execute();
                }
                break;
            }
        }
    }

    public class SaveTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(PopupDivision.this);

        @Override
        protected void onPreExecute() {


            for(int i=0; i<list.size(); i++) {
                DivisionPopData data = list.get(i);
                if(data.isChk()) {
                    new SOAPLoadTask(activity, activity).execute("usp_MOB_OutDivision_iud", SOAPLoadTask.convertParams("UP", userid, data.getDivisionNo(), data.getDivisionSeq(), String.valueOf(data.getItemQty())));
                }
            }

            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("저장 중입니다..");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
            Toast.makeText((PopupDivision) PopupDivision.CONTEXT, "저장 했습니다.", Toast.LENGTH_SHORT).show();
            ((PopupDivision) PopupDivision.CONTEXT).saveEnd();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getKeyCode() == 277 || event.getKeyCode() == 278) {
            etScan.setText("");
            etScan.requestFocus();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if(event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            if(!"".equals(etScan.getText().toString())) {
                String barcode = etScan.getText().toString().replaceAll("(\r\n|\r|\n|\n\r)", "");
                Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                //etScan.setText("");
                for(DivisionPopData data : list) {
                    data.setScan(false);
                }
                boolean checked = false;
                for(int i=0; i<list.size(); i++) {
                    DivisionPopData data = list.get(i);
                    if(barcode.equals(data.getBarcode())) {
                        checked = true;
                        //Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                        data.setScan(true);
                        data.setChk(true);
                        barcodeDivision = barcode;
                        listView.smoothScrollToPosition(i);
                        //barcodeDivisionIndex = i;
                        //adapter.notifyDataSetChanged();
                        //break;
                    }
                }
                if(checked) {
                    adapter.notifyDataSetChanged();
                }
                return false;
            }
        }


        return super.dispatchKeyEvent(event);

    }


}



