package com.coup.wms.screen;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.R;
import com.coup.wms.adapter.ListViewAdapter;
import com.coup.wms.data.ItemData;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by 신동현 on 2018-11-09.
 */
public class orderlist extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener {
    TextView sDate;
    final Calendar cldr = Calendar.getInstance();
    DatePickerDialog picker;
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);
    int dayOfWeek = cldr.get(Calendar.DAY_OF_WEEK);
    String userid;
    String _param;
    public static Context CONTEXT;
    View ping;
    ListView m_oListView;
    Button ordering;
    SharedPreferences setting;
    SharedPreferences.Editor editor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderlist);
        Intent intent = getIntent();
        sDate = findViewById(R.id.sdate);
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        tuserid.setText(userid + "님");
        CONTEXT = this;
        ordering = findViewById(R.id.ordering);
        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();
        ping = findViewById(R.id.ping);
      //  pingtest();
        ordering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(orderlist.this, ordering.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });



        _param = userid;
        setDate();
        onClick(null);
        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                Intent intent = new Intent(orderlist.this, loginActivity.class);
                                startActivity(intent);
                               onStop();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        m_oListView = (ListView) findViewById(R.id.listview);


    }

    @Override
    public void onResume() {
        super.onResume();

       onClick(null);

    }

    @Override
    public void onGetResult(String processer, SoapObject _result) {

        ArrayList<ItemData> oData = new ArrayList<>();
        try {
            for (int i = 1; i < _result.getPropertyCount(); i++) {
                SoapObject rs = (SoapObject) _result.getProperty(i);

                String cust_cd = AselTran.GetValue(rs, "cust_cd ");
                String cust_nm = AselTran.GetValue(rs, "cust_nm");                // 결과
                String arrival_nm = AselTran.GetValue(rs, "arrival_nm");
                String addr = AselTran.GetValue(rs, "addr");
                String seq_no = AselTran.GetValue(rs, "seq_no");
                String delivery_tm = AselTran.GetValue(rs, "delivery_tm");
                ItemData oItem = new ItemData();

                oItem.cust_cd = cust_cd;
                oItem.cust_nm = cust_nm;
                oItem.arrival_nm = arrival_nm;
                oItem.addr = addr;
                oItem.seq_no = seq_no;
                oItem.delivery_tm = delivery_tm;


                oData.add(oItem);
            }
            int list_amount = _result.getPropertyCount();
            Log.wtf("ik",String.valueOf(list_amount));
            if(list_amount==1){
                Toast.makeText(this, "배송 리스트가 없습니다.", Toast.LENGTH_LONG).show();
            }
            ListView m_oListView = (ListView) findViewById(R.id.listview);
            ListAdapter oAdapter = new ListViewAdapter(oData);
            m_oListView.setAdapter(oAdapter);
            ((ListViewAdapter) oAdapter).notifyDataSetChanged();

            m_oListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(orderlist.this, popup_order.class);
                    intent.putExtra("userid", userid);
                    intent.putExtra("position", position);
                    intent.putExtra("sdate", sDate.getText().toString());
                    startActivity(intent);
                }
            });


        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

            }
        }
    }

    @Override
    public void onClick(View v) {
        new SOAPLoadTask((SOAPLoadTask.OnResultListener) this, this).execute("usp_MOB_DeliveryTarget_list", SOAPLoadTask.convertParams(_param, "", sDate.getText().toString()));
    }

    public void backbtn(View v) {
        onBackPressed();
    }
    public void pingtest(){
        String host = "mall.ppang.biz";
        String cmd = "ping -c 1 -W 10 "+ host;
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            proc.waitFor();
            int result = proc.exitValue();
            if(result == 2)
                ping.setBackgroundColor(Color.parseColor("#FF3636"));
            else if(result == 1)
                ping.setBackgroundColor(Color.parseColor("#FFBB00"));


        } catch (Exception e) {
            Log.e("ping", e.getMessage());
        }
    }

    public void setDate(){

        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new Date());
        if(dayOfWeek==1)
        cal.add(Calendar.DAY_OF_YEAR, -2);
        else if(dayOfWeek==2)
            cal.add(Calendar.DAY_OF_YEAR, -3);
        else
            cal.add(Calendar.DAY_OF_YEAR, -1);

        SimpleDateFormat fm = new SimpleDateFormat(
                "yyyy-MM-dd");
        String strDate = fm.format(cal.getTime());
        sDate.setText(strDate);

    }

    public void setDate(View v) {



        switch (v.getId()) {
            case R.id.sdate:
                picker = new DatePickerDialog(orderlist.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                sDate.setText(year + "-" + String.format("%02d",monthOfYear + 1) + "-" + String.format("%02d",dayOfMonth));
                                onClick(null);
                            }
                        }, year, month, day);
                picker.show();
                break;

        }
    }

}
