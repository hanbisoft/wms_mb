package com.coup.wms.screen;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.R;
import com.coup.wms.adapter.ListViewAdapter_ordering;
import com.coup.wms.data.ItemData;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;


public class ordering extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener {
    String _param, userid;
    int startTime_p = 0, startHour_p, startMin_p, endMin_p, endTime_p = 0, endHour_p, betweenTime_p = 0, list_amount, pos = -1,debug_1=-1;
    ListView m_oListView;
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    Spinner chooseEndTime;
    Spinner chooseStartTime;
    boolean check, send_msg;
    ArrayList<ItemData> oData = new ArrayList<>();
    public static final String PREFS_NAME = "MyPrefsFile1";
    public CheckBox dontShowAgain;
    String gettime;
    Boolean check_equal = false;
    SimpleDateFormat dateFormat = new SimpleDateFormat("HHmm");
    Date reqDate, curDate;
    ListAdapter oAdapter;
    TextView nowtime;
    ImageButton seq_up;
    ImageButton seq_down;
    View ping;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ordering);
        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        seq_up = findViewById(R.id.seq_up);
        seq_down = findViewById(R.id.seq_down);
        nowtime = (TextView) findViewById(R.id.nowTime);
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String getTime = sdf.format(date);
        nowtime.setText(getTime);
        tuserid.setText(userid + "님");
        ping = findViewById(R.id.ping);
     //   pingtest();
        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();
        check = false;
        send_msg = false;
        _param = userid;

        onClick(null);


        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.change_pw:
                                Intent intent = new Intent(ordering.this, pw_change.class);
                                intent.putExtra("userid", userid);
                                startActivity(intent);
                                break;
                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                intent = new Intent(ordering.this, loginActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        AlertDialog.Builder alert_confirm = new AlertDialog.Builder(ordering.this);
        alert_confirm.setMessage("메세지 전송 하시겠습니까?").setCancelable(false).setPositiveButton("전송",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        send_msg = true;
                        CheckTypesTask task = new CheckTypesTask();
                        task.execute();
                        // 'YES'
                    }
                }).setNegativeButton("취소",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 'No'
                        return;
                    }
                });

        final AlertDialog alert = alert_confirm.create();
        findViewById(R.id.btn_saving).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check = true;
                CheckTypesTask task = new CheckTypesTask();
                task.execute();
            }
        });

        findViewById(R.id.send_msg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.show();
            }
        });


        m_oListView = (ListView) findViewById(R.id.listview);

        chooseEndTime = findViewById(R.id.end_time);
        chooseStartTime = findViewById(R.id.start_time);
        chooseStartTime.setSelection(8);
        chooseEndTime.setSelection(17);
        //얼럿다이얼로그
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        LayoutInflater adbInflater = LayoutInflater.from(this);
        View eulaLayout = adbInflater.inflate(R.layout.checkbox, null);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        final String skipMessage = settings.getString("skipMessage", "NOT checked");

        dontShowAgain = (CheckBox) eulaLayout.findViewById(R.id.skip);
//        adb.setView(eulaLayout);
//        adb.setTitle("알림");
//        adb.setMessage("\n상단 두 시간을 모두 선택하면 배달 시간을 일괄로 배정합니다.\n배달 순서를 설정하신 뒤에 시간을 일괄로 배정 하시는 걸 권장드립니다");
//
//        adb.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                String checkBoxResult = "NOT checked";
//
//                if (dontShowAgain.isChecked()) {
//                    checkBoxResult = "checked";
//                }
//
//                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
//                SharedPreferences.Editor editor = settings.edit();
//
//                editor.putString("skipMessage", checkBoxResult);
//                editor.commit();
//
//                // Do what you want to do on "OK" action
//
//                return;
//            }
//        });
//
//
//        if (!skipMessage.equals("checked")) {
//            adb.show();
//        }


        chooseStartTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                startHour_p = position + 1;
                startTime_p = 0;
                startMin_p = (position * 60) + 0;
                debug_1++;
                try {
                    time_cal();

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        chooseEndTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                endHour_p = position + 1;
                endTime_p = 0;
                endMin_p = (position * 60) + 0;
                debug_1++;
                try {
                    time_cal();

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
/*
        chooseStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final TimePickerDialog timePickerDialog = new TimePickerDialog(ordering.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {

                            chooseStartTime.setText(String.format("%02d:%02d", hourOfDay, 0));
                            startHour_p = hourOfDay;
                            startTime_p = 0;
                            startMin_p = (hourOfDay * 60) + 0;

                        try {
                            time_cal();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, 0, 0, false);
                timePickerDialog.show();
            }
        });


        chooseEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TimePickerDialog timePickerDialog = new TimePickerDialog(ordering.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {

                        chooseEndTime.setText(String.format("%02d:%02d", hourOfDay, 0));
                        endHour_p = hourOfDay;
                        endTime_p = 0;
                        endMin_p = (hourOfDay * 60) + 0;

                        try {
                            time_cal();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, 0, 0, false);
                timePickerDialog.show();
            }
        });*/

        seq_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos == list_amount - 2 || pos == -1) return;
                oData.get(pos).backpos = false;
                Collections.swap(oData, pos, pos + 1);
                pos++;
                oData.get(pos).backpos = true;
                ((ListViewAdapter_ordering) oAdapter).notifyDataSetChanged();
                m_oListView.smoothScrollToPosition (pos);
            }

        });

        seq_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos == 0 || pos == -1) return;
                oData.get(pos).backpos = false;
                Collections.swap(oData, pos, pos - 1);
                pos--;
                oData.get(pos).backpos = true;
                //  m_oData.get(position).seq_no= m_oData.get(position).seq_no+1;
                ((ListViewAdapter_ordering) oAdapter).notifyDataSetChanged();
                m_oListView.smoothScrollToPosition (pos);
            }

        });


    }

    private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long l_position) {
            for (int i = 0; i < list_amount - 1; i++) {
                oData.get(i).backpos = false;
            }

            oData.get(position).backpos = true;
            pos = position;
            ((ListViewAdapter_ordering) oAdapter).notifyDataSetChanged();
        }
    };

    public class CheckTypesTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(
                ordering.this);

        @Override
        protected void onPreExecute() {
            if (send_msg == true) {
                for (int i = 0; i <= list_amount - 2; i++) {
                    new SOAPLoadTask(this, this).execute("usp_MOB_SendMessage", SOAPLoadTask.convertParams(userid, oData.get(i).order_nm));
                }
            } else {
                for (int i = 0; i <= list_amount - 2; i++) {
                    new SOAPLoadTask(this, this).execute("usp_MOB_DeliveryTarget_Seq", SOAPLoadTask.convertParams(userid, oData.get(i).order_nm, oData.get(i).seq_no, oData.get(i).delivery_tm));
                }
            }


            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            if (send_msg == true) {
                asyncDialog.setMessage("전송 중입니다..");
                send_msg = false;
            } else
                asyncDialog.setMessage("저장 중입니다..");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
            ((orderlist) orderlist.CONTEXT).onResume();
        }
    }

    @Override
    public void onGetResult(String processer, SoapObject _result) {
        if (check == true) {
            try {
                SoapObject rs = (SoapObject) _result.getProperty(1);

                String Flag = AselTran.GetValue(rs, "RET");                // 결과
                String rMsg = AselTran.GetValue(rs, "MSG");                // 메세지
                if (!Flag.equals("-1")) {
                    Toast.makeText(this, "저장 중 잠시만 기다려 주세요", Toast.LENGTH_LONG).show();
                    return;
                }


            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + "ERROR", Toast.LENGTH_SHORT).show();
            }
        } else {
            try {
                for (int i = 1; i < _result.getPropertyCount(); i++) {
                    SoapObject rs = (SoapObject) _result.getProperty(i);

                    String cust_cd = AselTran.GetValue(rs, "cust_cd ");
                    String cust_nm = AselTran.GetValue(rs, "cust_nm");                // 결과
                    String arrival_nm = AselTran.GetValue(rs, "arrival_nm");
                    String addr = AselTran.GetValue(rs, "addr");
                    String order_no = AselTran.GetValue(rs, "order_no");
                    String seq_no = AselTran.GetValue(rs, "seq_no");
                    String delivery_tm = AselTran.GetValue(rs, "delivery_tm");
                    if (delivery_tm.equals("") || delivery_tm.isEmpty())
                        delivery_tm = "00:00";
                    if (seq_no.equals("") || seq_no.equals("0"))
                        seq_no = "0";


                    ItemData oItem = new ItemData();
                    oItem.cust_cd = cust_cd;
                    oItem.cust_nm = cust_nm;
                    oItem.arrival_nm = arrival_nm;
                    oItem.addr = addr;
                    oItem.seq_no = seq_no;
                    oItem.order_nm = order_no;
                    oItem.delivery_tm = delivery_tm;

                    oData.add(oItem);
                }

                list_amount = _result.getPropertyCount();
                if(list_amount==1){
                    Toast.makeText(this, "배송 리스트가 없습니다.", Toast.LENGTH_LONG).show();
                }
                oAdapter = new ListViewAdapter_ordering(oData);

                m_oListView.setAdapter(oAdapter);
                m_oListView.setOnItemClickListener(mItemClickListener);


            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + "ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        new SOAPLoadTask( this, this).execute("usp_MOB_DeliveryTarget_list", SOAPLoadTask.convertParams(_param));
    }

    public void backbtn(View v) {
        onBackPressed();
    }

    public void pingtest(){
        String host = "mall.ppang.biz";
        String cmd = "ping -c 1 -W 10 "+ host;
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            proc.waitFor();
            int result = proc.exitValue();
            if(result == 2)
                ping.setBackgroundColor(Color.parseColor("#FF3636"));
            else if(result == 1)
                ping.setBackgroundColor(Color.parseColor("#FFBB00"));


        } catch (Exception e) {
            Log.e("ping", e.getMessage());
        }
    }

    public void time_cal() throws ParseException {

        if(list_amount==1){
       return;
        }

        int hap, temp;

        try {
            reqDate = dateFormat.parse(String.format("%02d%02d", startHour_p, startTime_p));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long reqDateTime = reqDate.getTime();

        try {
            curDate = dateFormat.parse(String.format("%02d%02d", endHour_p, endTime_p));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long curDateTime = curDate.getTime();

        long minute = (curDateTime - reqDateTime) / 60000;
        betweenTime_p = Math.abs((int) minute) / (list_amount - 1);
        first:
        for (int i = 0; i <= list_amount - 2; i++) {
            if (Integer.valueOf(oData.get(i).seq_no) > list_amount) {
                break first;
            }
            hap = startTime_p + betweenTime_p * i;
            temp = startHour_p;
            while (hap >= 60) {
                hap -= 60;
                temp++;
                if (temp > 24)
                    temp = 0;
            }

            gettime = String.format("%02d:%02d", temp, hap);
            oData.get(i).delivery_tm = gettime;


        }

if(debug_1>=2){
    ((ListViewAdapter_ordering) oAdapter).notifyDataSetChanged();
}
    }
}




