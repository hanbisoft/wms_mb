package com.coup.wms.screen;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.R;
import com.coup.wms.adapter.ListViewAdapterPickPop;
import com.coup.wms.adapter.ListViewAdapterPileupPop;
import com.coup.wms.data.ComCode;
import com.coup.wms.data.CustLocData;
import com.coup.wms.data.PickData;
import com.coup.wms.data.PickPopData;
import com.coup.wms.data.PileUpPopData;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;

/**
 * Created by 신동현 on 2018-11-12.
 */
public class PopupPick extends Activity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener  {
    PopupPick activity;
    public static Context CONTEXT;

    String barcodeLoc = "";
    int barcodeLocIndex = -1;
    EditText etScan = null;

    TextView tvItemNm, tvBoxEaQty, tvOutlocCd;
    Button btnSave;
    String _param, order_no,sdate;

    int pos;
    String userid;
    String itemCd, itemNm;
    int ea_qty, box_qty;

    String slipNo;

    ListView listView;
    private ArrayList<PickPopData> list = new ArrayList<>();
    ListViewAdapterPickPop adapter;

    ArrayList<CustLocData> listCustLoc = new ArrayList<>();

    CheckBox chkAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_pick);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        activity = this;
        CONTEXT = this;

        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        pos = intent.getIntExtra("position", 0);
        sdate = intent.getStringExtra("sdate");

        itemCd = intent.getStringExtra("item_cd");
        itemNm = intent.getStringExtra("item_nm");
        //outloc_cd = intent.getStringExtra("outloc_cd");
        ea_qty = intent.getIntExtra("ea_qty", 0);
        box_qty = intent.getIntExtra("box_qty", 0);
        _param= userid;

        tvItemNm = findViewById(R.id.tv_item_nm);
        tvItemNm.setText(itemNm);

        //tvOutlocCd = findViewById(R.id.tv_outloc_cd);
        //tvOutlocCd.setText(outloc_cd);

        tvBoxEaQty = findViewById(R.id.tv_box_ea_qty);
        tvBoxEaQty.setText(box_qty + " / " + ea_qty);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);

        listView = (ListView)findViewById(R.id.listview);
        adapter = new ListViewAdapterPickPop(this, list, listCustLoc);
        listView.setAdapter(adapter);

        etScan = findViewById(R.id.tv_scan);

        chkAll = findViewById(R.id.chk_all);
        chkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for(int i=0; i<list.size(); i++) {
                    list.get(i).setChk(b);
                }
                if(adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        searchLoc();
    }

    private void searchLoc() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetPickInloc", SOAPLoadTask.convertParams("LIST"));
    }

    private void searchList() {
        new SOAPLoadTask(this, this).execute("usp_MOB_Pick_list", SOAPLoadTask.convertParams(
                "LIST3",
                "N",
                sdate,
                sdate,
                itemCd));
    }


    @Override
    public void onGetResult(String processer, SoapObject _result)
    {
        switch (processer) {
            case "usp_MOB_GetPickInloc" : {
                listCustLoc.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        CustLocData data = new CustLocData();
                        data.setLoc_cd(AselTran.GetValue(rs, "loc_cd"));
                        data.setLoc_nm(AselTran.GetValue(rs, "loc_nm"));
                        data.setBarcode(AselTran.GetValue(rs, "barcode"));

                        listCustLoc.add( data );
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                searchList();

                break;
            }

            case "usp_MOB_Pick_list": {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        PickPopData data = new PickPopData();
                        data.setChk("Y".equals(AselTran.GetValue(rs, "chk")));
                        data.setPick_no(AselTran.GetValue(rs, "pick_no"));
                        data.setPick_seq(AselTran.GetValue(rs, "pick_seq"));
                        data.setBarcode(AselTran.GetValue(rs, "barcode"));
                        data.setLotNo(AselTran.GetValue(rs, "lot_no"));
                        data.setItemQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setEaQty((int)Double.parseDouble(AselTran.GetValue(rs, "ea_qty")));
                        data.setBoxQty((int)Double.parseDouble(AselTran.GetValue(rs, "box_qty")));
                        data.setOutlocCd(AselTran.GetValue(rs, "outloc_cd"));
                        data.setInlocCd(AselTran.GetValue(rs, "inloc_cd"));
                        data.setDelvDegreeNm(AselTran.GetValue(rs, "delv_degree_nm"));
                        data.setCouresNm(AselTran.GetValue(rs, "coures_nm"));

                        list.add(data);
                    }

                    adapter = new ListViewAdapterPickPop(this, list, listCustLoc);
                    listView.setAdapter(adapter);
                }
                catch (Exception e) {
                    Toast.makeText(this,  e.getMessage() , Toast.LENGTH_SHORT).show();
                }
                break;
            }

        }
    }


    public void saveEnd() {
        setResult(RESULT_OK);
        finish();
    }


    @Override
    public void onError(SoapObject _result)
    {
        Toast.makeText(this,"통신에 실패하였습니다.",Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save: {
                int cnt = 0;
                for(int i=0; i<list.size(); i++) {
                    PickPopData data = list.get(i);
                    if(data.isChk()) {
                        cnt++;
                    }
                }
                if(cnt == 0) {
                    Toast.makeText(this, "저장할 작업이 없습니다.", Toast.LENGTH_SHORT).show();
                }
                else {
                    SaveTask task = new SaveTask();
                    task.execute();
                }
                break;
            }
        }
    }

    public class SaveTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(PopupPick.this);

        @Override
        protected void onPreExecute() {


            for (int i = 0; i < list.size(); i++) {
                PickPopData data = list.get(i);
                if (data.isChk()) {
                    new SOAPLoadTask(activity, activity).execute("usp_MOB_Picklist_iud", SOAPLoadTask.convertParams(
                            "IN",  data.getPick_no(), data.getPick_seq(), userid, data.getInlocCd()
                    ));
                }
            }

            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("저장 중입니다..");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
            Toast.makeText((PopupPick) PopupPick.CONTEXT, "저장 했습니다.", Toast.LENGTH_SHORT).show();
            ((PopupPick) PopupPick.CONTEXT).saveEnd();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getKeyCode() == 277 || event.getKeyCode() == 278) {
            etScan.setText("");
            etScan.requestFocus();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if(event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            if(!"".equals(etScan.getText().toString())) {
                String barcode = etScan.getText().toString().replaceAll("(\r\n|\r|\n|\n\r)", "");
                Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                //etScan.setText("");
                for(PickPopData data : list) {
                    data.setScan(false);
                }

                for(int i=0; i<list.size(); i++) {
                    PickPopData data = list.get(i);
                    if(barcode.equals(data.getBarcode())) {
                        //Toast.makeText(this, barcode, Toast.LENGTH_SHORT).show();
                        data.setScan(true);
                        data.setChk(true);
                        barcodeLoc = barcode;
                        listView.smoothScrollToPosition(i);
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
                return false;
            }
        }


        return super.dispatchKeyEvent(event);

    }


}



