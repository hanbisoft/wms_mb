package com.coup.wms.screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.wms.BuildConfig;
import com.coup.wms.R;
import com.coup.wms.util.AselTran;
import com.coup.wms.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;



public class pw_change extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener {

   TextView pw ;


    Button m_btnLogin;


    TextView new_pw;
    Toast m_toast;

    CheckBox chk_auto;
    SharedPreferences setting;
    SharedPreferences.Editor editor;

    String user_id ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pw_changer);
        final Intent intent = getIntent();
        user_id = intent.getStringExtra("userid");
        new_pw = findViewById(R.id.new_pw);
        m_btnLogin = findViewById(R.id.login_btn);
        pw =findViewById(R.id.pw);
        chk_auto = (CheckBox) findViewById(R.id.Auto_Login);
        setting = getSharedPreferences("setting", 0);
        editor= setting.edit();





        if (BuildConfig.DEBUG)
        {
            //m_textID.setText("test2@hotmail.com");
            //m_textPassword.setText("1234");
        }

        try {
            m_btnLogin.setOnClickListener(this);

        }
        catch (Exception err)
        {

        }

    }



    public void onClick(View _v)
    {
        if(pw.length()==0)
        {
            if(m_toast!=null)
                m_toast.cancel();
            m_toast= Toast.makeText(this,"비밀번호를 입력하세요.",Toast.LENGTH_SHORT);
            m_toast.show();

            pw.requestFocus();
            return;
        }

        if(new_pw.length()==0)
        {
            if(m_toast!=null)
                m_toast.cancel();

            m_toast= Toast.makeText(this,"새로운 비밀번호를 입력하세요.",Toast.LENGTH_SHORT);
            m_toast.show();

            new_pw.requestFocus();
            return;
        }

        List<String> _param = new ArrayList<String>();
        _param.add(user_id);
        _param.add(pw.getText().toString());
        _param.add(new_pw.getText().toString());


        //프로시저 == 틀 제목
        new SOAPLoadTask((SOAPLoadTask.OnResultListener) this, this).execute("usp_MOB_Password_chg", SOAPLoadTask.convertParams(_param));
    }

    @Override
    public void onGetResult(String processer, SoapObject _result)
    {
        SoapObject rs = (SoapObject)_result.getProperty(1);

        String Flag = AselTran.GetValue(rs, "RET");				// 결과
        String rMsg = AselTran.GetValue(rs, "MSG");				// 메세지
        if (Flag.equals("-1")) {
            Toast.makeText(this, "비밀번호가 서로 다르거나 오류가 발생했습니다.", Toast.LENGTH_SHORT).show();
            return;
        }
        editor.clear();
        editor.commit();
        finish();

    }

    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this,_result+"통신에 실패하였습니다.",Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
