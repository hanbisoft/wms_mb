package com.coup.wms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.CustLocData;

import java.util.List;

public class CustLocAdapter extends BaseAdapter {

    Context context;
    Object parentData;
    List<CustLocData> list;
    LayoutInflater inflater;


    public CustLocAdapter(Context context, Object parentData, List<CustLocData> data){
        this.context = context;
        this.parentData = parentData;
        this.list = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if(list!=null) return list.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.spinner_custloc, parent, false);
        }

        //데이터세팅
        CustLocData data = list.get(position);
        ((TextView)convertView.findViewById(R.id.tv_loc_nm)).setText(data.getLoc_nm());
        ((TextView)convertView.findViewById(R.id.tv_cust_nm)).setText(data.getCust_nm());

        convertView.setTag(parentData);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.spinner_custloc, parent, false);
        }

        //데이터세팅
        CustLocData data = list.get(position);
        ((TextView)convertView.findViewById(R.id.tv_loc_nm)).setText(data.getLoc_nm());
        if(data.getCust_nm() != null && !"".equals(data.getCust_nm())) {
            View sepa = convertView.findViewById(R.id.v_sepa);
            sepa.setVisibility(View.VISIBLE);
        }
        ((TextView)convertView.findViewById(R.id.tv_cust_nm)).setText(data.getCust_nm());

        convertView.setTag(parentData);

        return convertView;
    }

    @Override
    public CustLocData getItem(int position) {
        if(list != null && list.size() > 0) {
            return list.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}