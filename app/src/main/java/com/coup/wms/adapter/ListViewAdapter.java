package com.coup.wms.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.ItemData;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<ItemData> m_oData = null;
    private int nListCnt = 0;

    public ListViewAdapter(ArrayList<ItemData> _oData) {
        m_oData = _oData;
        nListCnt = m_oData.size();
    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder
    {

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            final Context context = parent.getContext();
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_order_list, parent, false);
        }

        LinearLayout backc = (LinearLayout) convertView.findViewById(R.id.backc);
        // LinearLayout order_pop = (LinearLayout) convertView.findViewById(R.id.order_pop);
        TextView order_nm = (TextView) convertView.findViewById(R.id.order_nm);
        TextView addr = (TextView) convertView.findViewById(R.id.addr);
        //TextView cust_nm = (TextView) convertView.findViewById(R.id.cust_nm);
        TextView arrival_nm = (TextView) convertView.findViewById(R.id.arrival_nm);


        if (position % 2 == 0)
            backc.setBackgroundColor(Color.parseColor("#f1f1f1"));
        else
            backc.setBackgroundColor(Color.parseColor("#ffffff"));

        if ((m_oData.get(position).seq_no).equals("")) {
            order_nm.setText("0");
        }
        else {
            order_nm.setText(m_oData.get(position).seq_no);
        }
        addr.setText(m_oData.get(position).addr);

        while ((m_oData.get(position).arrival_nm).length() >= 6){
            m_oData.get(position).arrival_nm = m_oData.get(position).arrival_nm.substring(0, m_oData.get(position).arrival_nm.length()-1);
        }
        arrival_nm.setText((m_oData.get(position).arrival_nm) +"\n" + m_oData.get(position).delivery_tm);


        return convertView;
    }
}