package com.coup.wms.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.CustLocData;
import com.coup.wms.data.PileUpData;
import com.coup.wms.util.AllSpinner;

import java.util.ArrayList;

public class ListViewAdapterPileUp extends BaseAdapter{
    LayoutInflater inflater = null;
    private Context context;
    private ArrayList<PileUpData> m_oData = null;
    //private ArrayList<CustLocData> custLocList = null;

    public ListViewAdapterPileUp(Context context, ArrayList<PileUpData> pileUpList) {
        this.context = context;
        m_oData = pileUpList;
        //this.custLocList = custLocList;
    }

    @Override
    public int getCount() {
        return m_oData == null ? 0 : m_oData.size();
    }

    @Override
    public PileUpData getItem(int position) {
        if(m_oData != null && m_oData.size() > 0) {
            return m_oData.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PileUpData data = getItem(position);

        final ViewHolder vh;
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_pileup, parent, false);
            vh = new ViewHolder();
            vh.backc = convertView.findViewById(R.id.backc);
//            vh.chk = convertView.findViewById(R.id.chk);
//            vh.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                    int position = vh.position;
//                        if(position >= 0) {
//                        m_oData.get(position).setChk(b);
//                    }
//                }
//            });
//            vh.chk.setTag(position);
            vh.tvItemNm = convertView.findViewById(R.id.tv_item_nm);
            vh.tvBoxEaQty = convertView.findViewById(R.id.tv_box_ea_qty);
//            vh.etItemQty = convertView.findViewById(R.id.et_item_qty);
//            vh.etItemQty.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                    int position = vh.position;
//                    if(position >= 0) {
//                        int itemQty = charSequence == null ? 0 : "".equals(charSequence.toString()) ? 0 : Integer.parseInt(charSequence.toString());
//                        m_oData.get(position).setItemQty(itemQty);
//                    }
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable editable) {
//                }
//            });
//            vh.etItemQty.setTag(position);
//            vh.spInlocCd = convertView.findViewById(R.id.sp_inloc_cd);
//            vh.adInlocCd = new CustLocAdapter(context, data, custLocList);
//            vh.spInlocCd.setOnItemSelectedListener(this);
//            vh.spInlocCd.setAdapter(vh.adInlocCd);
            vh.data = data;
            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder)convertView.getTag();
        }

        vh.position = position;

        if(data.isScan()) {
            vh.backc.setBackgroundColor(Color.YELLOW);
        }
        else {
            if (position % 2 == 0)
                vh.backc.setBackgroundColor(Color.parseColor("#f1f1f1"));
            else
                vh.backc.setBackgroundColor(Color.parseColor("#ffffff"));
        }


        vh.tvItemNm.setText(data.getItemNm());

//        vh.chk.setChecked(data.isChk());
//
//        int index = -1;
//        for(int i=0; i<custLocList.size(); i++) {
//            CustLocData loc = custLocList.get(i);
//            if(data.getInlocCd().equals(loc.getLoc_cd())) {
//                index = i;
//                break;
//            }
//        }
//        if(index > -1) {
//            vh.spInlocCd.setSelection(index);
//        }
//
//        vh.etItemQty.setText(data.getItemQty() + "");
        vh.tvBoxEaQty.setText(data.getBoxQty() + " / " + data.getEaQty());

        return convertView;
    }

//    @Override
//    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//
//        if(view != null) {
//            PileUpData data = (PileUpData)view.getTag();
//            data.setInlocCd(custLocList.get(i).getLoc_cd());
//        }
//
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> adapterView) {
//
//    }





    private class ViewHolder
    {
        int position;
        LinearLayout backc;
        //CheckBox chk;
        TextView tvItemNm;
        TextView tvBoxEaQty;
        //EditText etItemQty;
        //AllSpinner spInlocCd;
        //CustLocAdapter adInlocCd;
        PileUpData data;
    }
}