package com.coup.wms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.DefectData;

import java.util.List;
public class DefectAdapter extends BaseAdapter {

    Context context;
    View parentView;
    List<DefectData> list;
    LayoutInflater inflater;


    public DefectAdapter(Context context, View parentView, List<DefectData> data){
        this.context = context;
        this.parentView = parentView;
        this.list = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if(list!=null) return list.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.spinner_defect_gbn, parent, false);
        }

        //데이터세팅
        DefectData data = list.get(position);
        ((TextView)convertView.findViewById(R.id.tv_loc_nm)).setText(data.getCode_nm());
        ((TextView)convertView.findViewById(R.id.tv_cust_nm)).setText(data.getCust_nm());

        convertView.setTag(parentView);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.spinner_defect_gbn, parent, false);
        }

        //데이터세팅
        DefectData data = list.get(position);
        ((TextView)convertView.findViewById(R.id.tv_loc_nm)).setText(data.getCode_nm());
        if(data.getCust_nm() == null || "".equals(data.getCust_nm())) {
            View sepa = convertView.findViewById(R.id.v_sepa);
            sepa.setVisibility(View.INVISIBLE);
        }
        ((TextView)convertView.findViewById(R.id.tv_cust_nm)).setText(data.getCust_nm());

        convertView.setTag(parentView);

        return convertView;
    }

    @Override
    public DefectData getItem(int position) {
        if(list != null && list.size() > 0) {
            return list.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
