package com.coup.wms.adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.ItemData;

import java.util.ArrayList;


public class ListViewAdapter_ordering extends BaseAdapter {
    LayoutInflater inflater = null;
    public ArrayList<ItemData> m_oData = null;
    private int nListCnt = 0;
    public int pos=-1;

    public ListViewAdapter_ordering(ArrayList<ItemData> _oData) {

        m_oData = _oData;
        nListCnt = m_oData.size();

    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_list, parent, false);
        }

        final LinearLayout backc = (LinearLayout) convertView.findViewById(R.id.backc);
        final TextView order_nm = (TextView) convertView.findViewById(R.id.order_nm);
        TextView addr = (TextView) convertView.findViewById(R.id.addr);
        final TextView arrival_nm = (TextView) convertView.findViewById(R.id.arrival_nm);

        final View testView = convertView;

        final TimePickerDialog timePickerDialog;
//        if(!(m_oData.get(position).delivery_tm.equals("")) && !(m_oData.get(position).delivery_tm==null))
//        {
//
//
//            arrival_nm.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //arrival_nm.setText();
//                    final TimePickerDialog timePickerDialog = new TimePickerDialog(testView.getContext(), new TimePickerDialog.OnTimeSetListener() {
//                        @Override
//                        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
//                            String time = "";
//                            m_oData.get(position).delivery_tm = String.format("%02d:%02d",hourOfDay, minutes);
//                            arrival_nm.setText(String.format("%02d:%02d",hourOfDay, minutes));
//                            notifyDataSetChanged();
//                        }
//                    }, Integer.valueOf(m_oData.get(position).delivery_tm.substring(0, (m_oData.get(position).delivery_tm.indexOf(":")))), Integer.valueOf(m_oData.get(position).delivery_tm.substring((m_oData.get(position).delivery_tm.indexOf(":")) + 1)), false);
//                    timePickerDialog.show();
//
//
//                }
//
//            });
//        }



        order_nm.setFocusable(true);

        order_nm.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    final EditText Caption = (EditText) v;
                    m_oData.get(position).seq_no = Caption.getText().toString();
                }

            }
        });


        if(m_oData.get(position).backpos == true)
            backc.setBackgroundColor(Color.parseColor("#ffd400"));
        else if (m_oData.get(position).backpos == false && position % 2 == 0)
            backc.setBackgroundColor(Color.parseColor("#f1f1f1"));
        else
            backc.setBackgroundColor(Color.parseColor("#ffffff"));
       // if ((m_oData.get(position).seq_no).equals("") || m_oData.get(position).seq_no.isEmpty()) {
       //     order_nm.setText("0");
      //  }

            order_nm.setText(String.valueOf(position+1));
        m_oData.get(position).seq_no= String.valueOf(position+1);
        addr.setText(m_oData.get(position).addr);

        while ((m_oData.get(position).arrival_nm).length() >= 6){
            m_oData.get(position).arrival_nm = m_oData.get(position).arrival_nm.substring(0, m_oData.get(position).arrival_nm.length()-1);
        }
        arrival_nm.setText((m_oData.get(position).arrival_nm) +"\n" + m_oData.get(position).delivery_tm);

        return convertView;
    }


}



