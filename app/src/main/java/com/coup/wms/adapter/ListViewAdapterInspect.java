package com.coup.wms.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.ComCode;
import com.coup.wms.data.InspectData;
import com.coup.wms.util.AllSpinner;

import java.util.ArrayList;
import java.util.List;
import android.widget.CompoundButton;
import android.text.Editable;
import android.text.TextWatcher;

public class ListViewAdapterInspect extends BaseAdapter implements AdapterView.OnItemSelectedListener {
    LayoutInflater inflater = null;
    private ArrayList<InspectData> m_oData = null;
    private List<ComCode> defectList;
    private Context context;

    public ListViewAdapterInspect(Context context, ArrayList<InspectData> _oData, List<ComCode> defectList) {
        this.context = context;
        m_oData = _oData;
        this.defectList = defectList;
    }

    @Override
    public int getCount() {
        return m_oData == null ? 0 : m_oData.size();
    }

    @Override
    public InspectData getItem(int position) {
        if(m_oData != null && m_oData.size() > 0) {
            return m_oData.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        InspectData data = getItem(position);

        final ViewHolder vh;
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_list_inspect, parent, false);
            vh = new ViewHolder();
            vh.backc = convertView.findViewById(R.id.backc);
            vh.chk = convertView.findViewById(R.id.chk);

            vh.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int position = vh.position;
                    if(position >= 0) {
                        m_oData.get(position).setChk(b);
                    }
                }
            });
            vh.chk.setTag(position);

            vh.tvItemNm = convertView.findViewById(R.id.tv_item_nm);
            vh.tvBoxEaQty = convertView.findViewById(R.id.tv_box_ea_qty);
            vh.etItemQty = convertView.findViewById(R.id.et_item_qty);

            vh.etItemQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int position = vh.position;
                    if(position >= 0) {
                        int itemQty = charSequence == null ? 0 : "".equals(charSequence.toString()) ? 0 : Integer.parseInt(charSequence.toString());
                        m_oData.get(position).setItemQty(itemQty);
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            vh.etItemQty.setTag(position);
            vh.spDefect = convertView.findViewById(R.id.sp_defect_gbn);
            vh.adDefect = new ComCodeAdapter(context, defectList);
            vh.spDefect.setAdapter(vh.adDefect);
            vh.spDefect.setOnItemSelectedListener(this);

            vh.llInlocCd = convertView.findViewById(R.id.ll_defect_gbn);
            vh.data = data;
            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder)convertView.getTag();
        }

        if (position % 2 == 0)
            vh.backc.setBackgroundColor(Color.parseColor("#f1f1f1"));
        else
            vh.backc.setBackgroundColor(Color.parseColor("#ffffff"));

        vh.chk.setChecked(data.isChk());
        vh.tvItemNm.setText(data.getItemNm());

        int index = -1;
        for(int i=0; i<defectList.size(); i++) {
            ComCode defect = defectList.get(i);
            if(data.getDefectGbn().equals(defect.getCommCd())) {
                index = i;
                break;
            }
        }
        if(index > -1) {
            vh.spDefect.setSelection(index);
        }
        vh.position = position;
        if(data.isScan()) {
            vh.backc.setBackgroundColor(Color.YELLOW);
        }
        else {
            if (position % 2 == 0)
                vh.backc.setBackgroundColor(Color.parseColor("#f1f1f1"));
            else
                vh.backc.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        vh.etItemQty.setText(data.getItemQty() + "");
        vh.tvBoxEaQty.setText(data.getBoxQty() + " / " + data.getEaQty());

        return convertView;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View convertView, int position, long l) {

        if(convertView != null) {
            if(adapterView.isPressed()) {
                InspectData data = (InspectData)convertView.getTag();
                data.setInlocCd(defectList.get(position).getCommCd());
            }

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class ViewHolder
    {
        int position;
        LinearLayout backc;
        CheckBox chk;
        TextView tvItemNm;
        TextView tvBoxEaQty;
        EditText etItemQty;
        AllSpinner spDefect;
        ComCodeAdapter adDefect;
        LinearLayout llInlocCd;
        InspectData data;
    }
}