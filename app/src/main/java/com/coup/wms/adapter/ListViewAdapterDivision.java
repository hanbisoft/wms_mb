package com.coup.wms.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.DivisionData;

import java.util.ArrayList;

public class ListViewAdapterDivision extends BaseAdapter {
    LayoutInflater inflater = null;
    private Context context;
    private ArrayList<DivisionData> m_oData = null;

    public ListViewAdapterDivision(Context context, ArrayList<DivisionData> list) {
        this.context = context;
        m_oData = list;
    }

    @Override
    public int getCount() {
        return m_oData == null ? 0 : m_oData.size();
    }

    @Override
    public DivisionData getItem(int position) {
        if(m_oData != null && m_oData.size() > 0) {
            return m_oData.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        DivisionData data = getItem(position);

        final ViewHolder vh;
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_division, parent, false);
            vh = new ViewHolder();
            vh.backc = convertView.findViewById(R.id.backc);
            vh.tvItemNm = convertView.findViewById(R.id.tv_item_nm);
            vh.tvBoxEaQty = convertView.findViewById(R.id.tv_box_ea_qty);
            vh.data = data;
            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder)convertView.getTag();
        }

        vh.position = position;

        if(data.isScan()) {
            vh.backc.setBackgroundColor(Color.YELLOW);
        }
        else {
            if (position % 2 == 0)
                vh.backc.setBackgroundColor(Color.parseColor("#f1f1f1"));
            else
                vh.backc.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        vh.tvItemNm.setText(data.getItemNm());

        vh.tvBoxEaQty.setText(data.getBoxQty() + " / " + data.getEaQty());

        return convertView;
    }


    private class ViewHolder
    {
        int position;
        LinearLayout backc;
        TextView tvItemNm;
        TextView tvBoxEaQty;
        DivisionData data;
    }
}