package com.coup.wms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.ComCode;

import java.util.List;

public class ComCodeAdapter  extends BaseAdapter {

    Context context;
    List<ComCode> data;
    LayoutInflater inflater;


    public ComCodeAdapter(Context context, List<ComCode> data){
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if(data!=null) return data.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.spinner_comcode, parent, false);
        }

        //데이터세팅
        ComCode comCode = data.get(position);
        ((TextView)convertView.findViewById(R.id.tv_code_nm)).setText(comCode.getCodeNm());

        convertView.setTag(comCode);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.spinner_comcode, parent, false);
        }

        //데이터세팅
        ComCode comCode = data.get(position);
        ((TextView)convertView.findViewById(R.id.tv_code_nm)).setText(comCode.getCodeNm());

        convertView.setTag(comCode);

        return convertView;
    }

    @Override
    public ComCode getItem(int position) {
        if(data != null && data.size() > 0) {
            return data.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}