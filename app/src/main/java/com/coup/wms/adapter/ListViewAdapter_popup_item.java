package com.coup.wms.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.wms.R;
import com.coup.wms.data.ItemData;

import java.util.List;

public class ListViewAdapter_popup_item extends ArrayAdapter<ItemData>
{

    LayoutInflater inflater = null;
    private List<ItemData> m_oData = null;
    private int mResource;
    private int nListCnt;

    public ListViewAdapter_popup_item(Context context, int resource, List<ItemData> _oData) {
        super(context, resource, _oData);
        m_oData = _oData;
        nListCnt = m_oData.size();
        mResource = resource;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            final Context context = parent.getContext();
            if (inflater == null)
            {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_order_item, parent, false);
        }

        LinearLayout backc = (LinearLayout) convertView.findViewById(R.id.backc);
        TextView item_nm = (TextView) convertView.findViewById(R.id.item_nm);
        EditText order_qty = (EditText) convertView.findViewById(R.id.order_qty);
        TextView price_amt =  (TextView) convertView.findViewById(R.id.price_amt);


        ItemData item = getItem(position);
        order_qty.setFocusable(true);

        order_qty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    final EditText Caption = (EditText) v;
                    m_oData.get(position).order_qty = Caption.getText().toString();
                }
            }
        });

        if(position%2==0)
            backc.setBackgroundColor(Color.parseColor("#ffffff"));
        else
            backc.setBackgroundColor(Color.parseColor("#f1f1f1"));

        item_nm.setText(m_oData.get(position).item_nm);
        order_qty.setText(m_oData.get(position).order_qty);
        price_amt.setText(m_oData.get(position).price_amt);

        return convertView;
    }
}
